@extends('adminlte::page')
@section('content')
<div class="container">
   <div class="row">
      <div class="col-md-9">
         <div class="panel panel-default">
            <div class="panel-heading">Agendamento</div>
            <div class="panel-body">
               <table class='table table-bordered table-striped'>
                  <thead>
                     <tr>
                        <th>
                           Hora
                        </th>
                        <th>
                           Disponível?
                        </th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($horarios as $key => $horario)
                     <tr>
                        <td>

                           {{ $horario -> hora }}
                        </td>
                        <td>

                           <select id="{{ $horario -> id }}" class="hr_disp">
                              <option value="0" {{ $horario -> disponivel == 0 ? 'selected' : ''}}>Não</option>
                              <option value="1" {{ $horario -> disponivel == 1 ? 'selected' : ''}}>Sim</option>
                           </select>
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
var token = $('meta[name="csrf-token"]').attr('content');
$('.hr_disp').change(function(){
   var id = $(this).attr('id');
   var disponivel = $(this).val();
      $.post('horarios_disponiveis',{_token: token, id: id, disponivel: disponivel}, function(){

      });
   });
</script>
@endsection
