@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Atividade</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/area-profissional/create') }}" class="btn btn-success btn-sm" title="Add New AreaProfissional">
                            <i class="fa fa-plus" aria-hidden="true"></i></a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/area-profissional', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>Descrição</th><th>Sigla Registro</th><th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($areaprofissional as $item)
                                    <tr>
                                        <td>{{ $item->descr_area }}</td><td>{{ $item->sigla_registro }}</td>
                                        <td>
                                            <a href="{{ url('/admin/area-profissional/' . $item->id) }}" title="Visualizar Área Profissional"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/area-profissional/' . $item->id . '/edit') }}" title="Editar  Área Profissional"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/area-profissional', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Deletar Área Profissional',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $areaprofissional->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
