@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Atividade</div>
                    <div class="panel-body">

                        <a href="{{ url('/admin/area-profissional') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i></button></a>
                        <a href="{{ url('/admin/area-profissional/' . $areaprofissional->id . '/edit') }}" title="Edit AreaProfissional"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/areaprofissional', $areaprofissional->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete AreaProfissional',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                       <th> Descrição </th>
                                       <td> {{ $areaprofissional->descr_area }} </td>
                                    </tr>
                                    <tr>
                                       <th> Sigla Registro </th>
                                       <td> {{ $areaprofissional->sigla_registro }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
