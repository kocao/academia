<div class="form-group {{ $errors->has('descr_area') ? 'has-error' : ''}}">
    {!! Form::label('descr_area', 'Descrição', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('descr_area', null, ['class' => 'form-control', 'required' => '']) !!}
        {!! $errors->first('descr_area', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('sigla_registro') ? 'has-error' : ''}}">
    {!! Form::label('sigla registro', 'Sigla Registro', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('sigla_registro', null, ['class' => 'form-control']) !!}
        {!! $errors->first('sigla_registro', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Criar', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
