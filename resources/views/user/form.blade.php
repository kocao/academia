<div class="form-group {{ $errors->has('perfil') ? 'has-error' : ''}}">
    {!! Form::label('perfil', 'Perfil', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('perfil', ['Administrador'=>'Administrador', 'Profissional'=>'Profissional', 'Aluno'=>'Aluno'],null,['class' => 'perfil']); !!}
        <!-- {!! Form::number('perfil', null, ['class' => 'form-control']) !!} -->
        {!! $errors->first('perfil', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Nome', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control','required'=>'']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('email', null, ['class' => 'form-control','required'=>'']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="admin-only">
   <div class="form-group {{ $errors->has('id_area_profissional') ? 'has-error' : ''}}">
       {!! Form::label('id_area_profissional', 'Area Profissional', ['class' => 'col-md-4 control-label']) !!}
       <div class="col-md-6">
           {!! Form::select('id_area_profissional', $areas_profissionais, null, ['class' => 'select2']); !!}
           {!! $errors->first('id_area_profissional', '<p class="help-block">:message</p>') !!}
       </div>
   </div>
   <div class="form-group {{ $errors->has('registro_profissional') ? 'has-error' : ''}}">
       {!! Form::label('registro_profissional', 'Registro Profissional', ['class' => 'col-md-4 control-label']) !!}
       <div class="col-md-6">
           {!! Form::text('registro_profissional', null, ['class' => 'form-control']) !!}
           {!! $errors->first('registro_profissional', '<p class="help-block">:message</p>') !!}
       </div>
   </div>
</div>
<div class="form-group {{ $errors->has('telefone') ? 'has-error' : ''}}">
    {!! Form::label('telefone', 'Telefone', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('telefone', null, ['class' => 'form-control', 'onkeyup' => 'window.telefone_mask($(this))', 'maxlength' => 15]) !!}
        {!! $errors->first('telefone', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('whatsap') ? 'has-error' : ''}}">
    {!! Form::label('whatsap', 'Whatsap', ['class' => 'col-md-4 control-label', ]) !!}
    <div class="col-md-6">
        {!! Form::text('whatsap', null, ['class' => 'form-control', 'onkeyup' => 'window.telefone_mask($(this))', 'maxlength' => 15]) !!}
        {!! $errors->first('whatsap', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('cpf') ? 'has-error' : ''}}">
    {!! Form::label('cpf', 'CPF', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('cpf', null, ['class' => 'form-control','required'=>'','maxlength' => 14, 'onkeyup' => 'window.cpf_mask($(this))']) !!}
        {!! $errors->first('cpf', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('dt_nasc') ? 'has-error' : ''}}">
    {!! Form::label('dt_nasc', 'Data de Nascimento', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('dt_nasc', null, ['class' => 'form-control','required'=>'','data-readonly' => '','id'=>'data']) !!}
        {!! $errors->first('dt_nasc', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('dt_nasc') ? 'has-error' : ''}}">
    {!! Form::label('ativo', 'Ativo', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('ativo',["1" => "Ativo", "0" => "Inativo"]) !!}
        {!! $errors->first('ativo', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<?php $action = explode('@', Route::currentRouteAction())[1] ?>
@if($action == 'create')
   <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
      {!! Form::label('password', 'Password', ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
           {!! Form::password('password', null, ['class' => 'form-control']) !!}
           {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
       </div>
   </div>
@endif

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Criar', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

@section('js')
<script>
$(document).ready(function(){
   $('#data').datepicker({
      dateFormat: 'dd/mm/yy',
      changeYear: true,
      yearRange: "-100:+0",
      showOn: "button",
      buttonImage: "{{asset('images/calendar.png')}}",
      buttonImageOnly: true,
      buttonText: "Selecione a data"
   });
   $('.perfil').change();
});
$('.perfil').change(function(){
   if($(this).val() == "Aluno"){
      $('.admin-only').hide();
   } else {
      $('.admin-only').show();
   }
});
</script>
@stop
