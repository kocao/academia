@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
              @if(Session::has('flash_error'))
              <div class="panel panel-danger">
                  <div class="panel-body bg-danger text-danger text-center">
                    {{ Session::get('flash_error') }}
                  </div>
              </div>
              @endif
              @if(Session::has('flash_message'))
              <div class="panel panel-success">
                  <div class="panel-body bg-success text-success text-center">
                    {{ Session::get('flash_message') }}
                  </div>
              </div>
              @endif

                <div class="panel panel-default">
                    <div class="panel-heading">Usuário</div>
                    <div class="panel-body">
                        <a href="{{ url('/user/create') }}" class="btn btn-success btn-sm" title="Novo User">
                            <i class="fa fa-plus" aria-hidden="true"></i></a>

                        {!! Form::open(['method' => 'GET', 'url' => '/user', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>Perfil</th><th>Name</th><th>Email</th><th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($user as $item)
                                    <tr>
                                        <td>{{ $item->perfil }}</td><td>{{ $item->name }}</td><td>{{ $item->email }}</td>
                                        <td>
                                            <a href="{{ url('/user/' . $item->id) }}" title="Visualizar User"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/user/' . $item->id . '/edit') }}" title="Editar User"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/user', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Deletar User',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $user->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
