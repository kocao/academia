@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Usuário</div>
                    <div class="panel-body">

                        <a href="{{ url('/user') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i></button></a>
                        <a href="{{ url('/user/' . $user->id . '/edit') }}" title="Editar User"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['user', $user->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete User',
                                    'onclick'=>'return confirm("Tem certeza?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                    </tr>
                                    <tr>
                                       <th> Perfil </th>
                                       <td> {{ $user -> perfil }} </td>
                                    </tr>
                                    <tr>
                                       <th> Nome </th>
                                       <td> {{ $user->name }} </td>
                                    </tr>
                                    <tr>
                                       <th> Email </th>
                                       <td> {{ $user->email }} </td>
                                    </tr>
                                    @if($user -> perfil != 'Aluno' )
                                    <tr>
                                       <th> Área profissional </th>
                                       <td> {{ empty ($user -> area_profissional) ? '': $user -> area_profissional -> descr_area }} </td>
                                    </tr>
                                    <tr>
                                       <th> Registro </th>
                                       <td> {{ $user -> registro }} </td>
                                    </tr>
                                    @endif

                                    <tr>
                                       <th> Telefone </th>
                                       <td> {{ $user -> telefone }} </td>
                                    </tr>
                                    <tr>
                                       <th> Whatsapp </th>
                                       <td> {{ $user -> whatsap }} </td>
                                    </tr>
                                    <tr>
                                       <th> CPF </th>
                                       <td> {{ $user -> cpf }} </td>
                                    </tr>
                                    <tr>
                                       <th> Data de Nascimento </th>
                                       <td> {{ $user -> dt_nasc }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
