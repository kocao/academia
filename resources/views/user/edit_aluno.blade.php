@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Dados Pessoais</div>
                    <div class="panel-body">
                        <a href="{{ url('/user') }}" title="Voltar"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i></button></a>
                        <br />
                        <br />

                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($user, [
                            'method' => 'PATCH',
                            'url' => ['/update-aluno'],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        <div class="form-group {{ $errors->has('telefone') ? 'has-error' : ''}}">
                            {!! Form::label('telefone', 'Telefone', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('telefone', null, ['class' => 'form-control', 'onkeyup' => 'window.telefone_mask($(this))', 'maxlength' => 15]) !!}
                                {!! $errors->first('telefone', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('whatsap') ? 'has-error' : ''}}">
                            {!! Form::label('whatsap', 'Whatsap', ['class' => 'col-md-4 control-label', ]) !!}
                            <div class="col-md-6">
                                {!! Form::text('whatsap', null, ['class' => 'form-control', 'onkeyup' => 'window.telefone_mask($(this))', 'maxlength' => 15]) !!}
                                {!! $errors->first('whatsap', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('cpf') ? 'has-error' : ''}}">
                            {!! Form::label('cpf', 'CPF', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('cpf', null, ['class' => 'form-control','required'=>'','maxlength' => 14, 'onkeyup' => 'window.cpf_mask($(this))']) !!}
                                {!! $errors->first('cpf', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="panel panel-danger">
                           <div class="panel-body bg-danger">
                              <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                 {!! Form::label('name', 'Nome', ['class' => 'col-md-4 control-label']) !!}
                                 <div class="col-md-6">
                                    {!! Form::text('name', null, ['class' => 'form-control','required'=>'']) !!}
                                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                 </div>
                              </div>
                              <div class="form-group {{ $errors->has('dt_nasc') ? 'has-error' : ''}}">
                                  {!! Form::label('dt_nasc', 'Data de Nascimento', ['class' => 'col-md-4 control-label']) !!}
                                  <div class="col-md-6">
                                      {!! Form::text('dt_nasc', null, ['class' => 'form-control','required'=>'','data-readonly' => '','id'=>'data']) !!}
                                      {!! $errors->first('dt_nasc', '<p class="help-block">:message</p>') !!}
                                  </div>
                              </div>
                              <div class="form-group {{ $errors->has('contato_emer') ? 'has-error' : ''}}">
                                 {!! Form::label('contato_emer', 'Contato para Emergência', ['class' => 'col-md-4 control-label']) !!}
                                 <div class="col-md-6">
                                    {!! Form::text('contato_emer', null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('contato_emer', '<p class="help-block">:message</p>') !!}
                                 </div>
                              </div>
                              <div class="form-group {{ $errors->has('medico') ? 'has-error' : ''}}">
                                 {!! Form::label('medico', 'Médico', ['class' => 'col-md-4 control-label']) !!}
                                 <div class="col-md-6">
                                    {!! Form::text('medico', null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('medico', '<p class="help-block">:message</p>') !!}
                                 </div>
                              </div>
                              <div class="form-group {{ $errors->has('tipo_sang') ? 'has-error' : ''}}">
                                 {!! Form::label('tipo_sang', 'Tipo sanguíneo', ['class' => 'col-md-4 control-label']) !!}
                                 <div class="col-md-6">
                                    {!! Form::text('tipo_sang', null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('tipo_sang', '<p class="help-block">:message</p>') !!}
                                 </div>
                              </div>
                              <div class="form-group {{ $errors->has('alergias') ? 'has-error' : ''}}">
                                 {!! Form::label('alergias', 'Alergias', ['class' => 'col-md-4 control-label']) !!}
                                 <div class="col-md-6">
                                    {!! Form::text('alergias', null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('alergias', '<p class="help-block">:message</p>') !!}
                                 </div>
                              </div>
                              <div class="form-group {{ $errors->has('plano_saude') ? 'has-error' : ''}}">
                                 {!! Form::label('plano_saude', 'Plano de Saúde', ['class' => 'col-md-4 control-label']) !!}
                                 <div class="col-md-6">
                                    {!! Form::text('plano_saude', null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('plano_saude', '<p class="help-block">:message</p>') !!}
                                 </div>
                              </div>
                              <div class="form-group {{ $errors->has('hospital') ? 'has-error' : ''}}">
                                 {!! Form::label('hospital', 'Hospital', ['class' => 'col-md-4 control-label']) !!}
                                 <div class="col-md-6">
                                    {!! Form::text('hospital', null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('hospital', '<p class="help-block">:message</p>') !!}
                                 </div>
                              </div>
                              <div class="form-group ">
                                 <div class="row">

                                 <div class="col-md-4">
                                 </div>
                                 <div class="col-md-6">
                                    {!! Form::checkbox('concordo',1,null) !!}
                                    Aceito tornar públicas as informações em destaque
                                 </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                {!! Form::submit('Atualizar', ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>


                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
<script>
$(document).ready(function(){
  $('#data').datepicker({
    dateFormat: 'dd/mm/yy',
    changeYear: true,
    yearRange: "-100:+0",
    showOn: "button",
    buttonImage: "{{asset('images/calendar.png')}}",
    buttonImageOnly: true,
    buttonText: "Selecione a data"
  });
  $('.perfil').change();
});
$('.perfil').change(function(){
  if($(this).val() == "Aluno"){
    $('.admin-only').hide();
  } else {
    $('.admin-only').show();
  }
});
</script>
@stop
