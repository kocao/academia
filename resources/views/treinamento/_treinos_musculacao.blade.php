@foreach($treinamento -> treinos as $key_treino => $treino)
  {!! Form::hidden("treino[$key_treino][id_treino]", $treino -> id) !!}
   <div class="table-responsive">

   <table class="table">
      <tbody>
      <tr>
         <td colspan="6" class="head-treino text-center">
            {{ Form::hidden("treino[$key_treino][sigla]", $treino -> sigla) }}

            {!! Form::text("treino[$key_treino][titulo_treino]", $treino -> titulo_treino, ['class' => 'text-center head-treino', 'readonly' => '']) !!}
         </td>
      </tr>
      <tr>
         <td class="head-treino">Exercício</td>
         <td class="head-treino">Série</td>
         <td class="head-treino">Repetição</td>
         <td class="head-treino">Carga</td>
         <td class="head-treino">Ajustes</td>
         <td class="head-treino">Pausa</td>
      </tr>
      @foreach($treino -> atividades_mus as $key_atividade => $atividade)
         @include('treinamento._atividade_mus')
      @endforeach
      </tbody>
   </table>
   </div>
@endforeach
