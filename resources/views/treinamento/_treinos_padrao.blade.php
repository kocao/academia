@foreach($treinamento -> treinos as $key_treino => $treino)
  {!! Form::hidden("treino[$key_treino][id_treino]", $treino -> id) !!}
   <table class="table">
      <tbody>
      <tr>
         <td colspan="4" class="head-treino text-center">
            {{ Form::hidden("treino[$key_treino][sigla]", $treino -> sigla) }}
            {!! Form::text("treino[$key_treino][titulo_treino]", $treino -> titulo_treino, ['class' => 'text-center head-treino', 'readonly' => '']) !!}
         </td>
      </tr>
      <tr>
         <td class="head-treino">{{ head_treino($treinamento -> tipo_treino)['atividade']}}</td>
         <td class="head-treino">{{ head_treino($treinamento -> tipo_treino)['distancia']}}</td>
         <td class="head-treino">Tempo</td>
         <td class="head-treino">Pace</td>
      </tr>
      @foreach($treino -> atividades as $key_atividade => $atividade)
         @include('treinamento._atividade')
      @endforeach

      <tr>
         <td colspan="4" class="text-center cell-treino">
            Anotações

         </td>
      </tr>
      <tr>
         <td colspan="4" class="col-md-12 cell-treino">
            {!! Form::textarea("treino[$key_treino][anotacoes]", $treino -> anotacoes, ['size' => '25x05', 'class' => 'data bg-transparente sem-borda']) !!}
         </td>
      </tr>
      </tbody>
   </table>
@endforeach
