<table class="table">
  <tbody>
    <td colspan="6" class="head-treino text-center">
      Treino A
    </td>
    <tr>
      <td class="head-treino">Público</td>
      <td class="head-treino">Aquecimento</td>
      <td class="head-treino">Tempo</td>
      <td class="head-treino">Aparelho</td>
      <td class="head-treino">Exercício Específico</td>
      <td class="head-treino">Volta a Calma</td>
    </tr>
    @foreach($treinamento -> treinos as $key_treino => $treino)
    <tr>
      <td colspan="6" class="head-treino text-center">
        {!!  $treino -> titulo_treino !!}
      </td>
    </tr>
            @foreach($treino -> atividades_pil as $key_atividade => $atividade)
            <tr>
               <td class="cell-treino">
                  {{ $atividade -> publico  }}
               </td>
               <td class="cell-treino">
                  {{ $atividade -> aquecimento }}
               </td>
               <td class="cell-treino">
                  {{ $atividade -> tempo }}
               </td>
               <td class="cell-treino">
                  {{ $atividade -> aparelho }}
               </td>
               <td class="cell-treino">
                  {{ $atividade -> exercicio }}
               </td>
               <td class="cell-treino">
                  {{ $atividade -> volta }}
               </td>
            </tr>
            @endforeach
      @endforeach
      </tbody>
   </table>
