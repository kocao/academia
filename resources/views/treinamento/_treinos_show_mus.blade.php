@foreach($treinamento -> treinos as $key_treino => $treino)
  <table class="table">
     <tbody>
     <tr>
        <td colspan="6" class="head-treino text-center">
           {{ $treino -> titulo_treino }}
        </td>
     </tr>
     <tr>
        <td class="head-treino">Exercício</td>
        <td class="head-treino">Série</td>
        <td class="head-treino">Repetição</td>
        <td class="head-treino">Carga</td>
        <td class="head-treino">Ajustes</td>
        <td class="head-treino">Pausa</td>
     </tr>
     @foreach($treino -> atividades_mus as $key_atividade => $atividade)
     <tr>
        <td class="cell-treino">
           {!! $atividade -> exercicio !!}
        </td>
        <td class="cell-treino">
           {!! $atividade -> serie !!}
        </td>
        <td class="cell-treino">
           {!! $atividade -> repeticao !!}
        </td>
        <td class="cell-treino">
           {!! $atividade -> carga !!}
        </td>
        <td class="cell-treino">
           {!! $atividade -> ajustes !!}
        </td>
        <td class="cell-treino">
           {!! $atividade -> pausa !!}
        </td>
     </tr>
     @endforeach
     </tbody>
  </table>
@endforeach
