@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Treinamento</div>
                    <div class="panel-body">
                       <div class="form-group {{ $errors->has('tipo_treino') ? 'has-error' : ''}}">
                           {!! Form::label('tipo_treino', 'Tipo de Treino', ['class' => 'col-md-4 control-label']) !!}
                           <div class="col-md-6"> <div class='form-control'>{{ $tipos_treino[$treinamento -> tipo_treino] }}</div> </div>
                       </div>
                       <div class="form-group {{ $errors->has('sublimiar1') ? 'has-error' : ''}}">
                           {!! Form::label('sublimiar1', 'Sublimiar1', ['class' => 'col-md-4 control-label']) !!}
                           <div class="col-md-6"> <div class='form-control'>{{ $treinamento -> sublimiar1 }}</div> </div>
                       </div>
                       <div class="form-group {{ $errors->has('sublimiar2') ? 'has-error' : ''}}">
                           {!! Form::label('sublimiar2', 'Sublimiar2', ['class' => 'col-md-4 control-label']) !!}
                           <div class="col-md-6"> <div class='form-control'>{{ $treinamento -> sublimiar2 }}</div> </div>
                       </div>
                       <div class="form-group {{ $errors->has('limiaraerobio') ? 'has-error' : ''}}">
                           {!! Form::label('limiaraerobio', 'Limiaraerobio', ['class' => 'col-md-4 control-label']) !!}
                           <div class="col-md-6"> <div class='form-control'>{{ $treinamento -> limiaraerobio }}</div> </div>
                       </div>
                       <div class="form-group {{ $errors->has('supralimiar') ? 'has-error' : ''}}">
                           {!! Form::label('supralimiar', 'Supralimiar', ['class' => 'col-md-4 control-label']) !!}
                           <div class="col-md-6"> <div class='form-control'>{{ $treinamento -> supralimiar }}</div> </div>
                       </div>
                       <div class="form-group {{ $errors->has('peso') ? 'has-error' : ''}}">
                           {!! Form::label('peso', 'Peso', ['class' => 'col-md-4 control-label']) !!}
                           <div class="col-md-6"> <div class='form-control'>{{ $treinamento -> peso }}</div> </div>
                       </div>
                       <div class="form-group {{ $errors->has('estatura') ? 'has-error' : ''}}">
                           {!! Form::label('estatura', 'Estatura', ['class' => 'col-md-4 control-label']) !!}
                           <div class="col-md-6"> <div class='form-control'>{{ $treinamento -> estatura }}</div> </div>
                       </div>
                       <div class="form-group {{ $errors->has('objetivo') ? 'has-error' : ''}}">
                           {!! Form::label('objetivo', 'Objetivo', ['class' => 'col-md-4 control-label']) !!}
                           <div class="col-md-6"> <div class='form-control'>{{ $treinamento -> objetivo }}</div> </div>
                       </div>
                       @include($view_treino)
                       @if($treinamento -> tipo_treino != 5)
                       <div class="table-responsive">
                          <table class="table table-bordered" style="background-color:#c8cace;">
                             <thead>
                                <tr>
                                   <th></th>
                                   <th>S</th>
                                   <th>T</th>
                                   <th>Q</th>
                                   <th>Q</th>
                                   <th>S</th>
                                   <th>S</th>
                                   <th>D</th>
                                </tr>
                             </thead>
                             <tbody>
                                @foreach($treinamento -> periodos_treinamento as $key_periodo => $periodo)
                                   <tr>
                                      <td>{{ $periodo -> semana }}</td>
                                      <td>
                                         <button type="button" class="btn btn-default btn-xs btn-treino">{{$periodo -> segunda}}</button>
                                      </td>
                                      <td>
                                         <button type="button" class="btn btn-default btn-xs btn-treino">{{$periodo -> terca}}</button>
                                      </td>
                                      <td>
                                         <button type="button" class="btn btn-default btn-xs btn-treino">{{$periodo -> quarta}}</button>
                                      </td>
                                      <td>
                                         <button type="button" class="btn btn-default btn-xs btn-treino">{{$periodo -> quinta}}</button>
                                      </td>
                                      <td>
                                         <button type="button" class="btn btn-default btn-xs btn-treino">{{$periodo -> sexta}}</button>
                                      </td>
                                      <td>
                                         <button type="button" class="btn btn-default btn-xs btn-treino">{{$periodo -> sabado}}</button>
                                      </td>
                                      <td>
                                         <button type="button" class="btn btn-default btn-xs btn-treino">{{$periodo -> domingo}}</button>
                                      </td>
                                   </tr>
                                @endforeach
                             </tbody>
                          </table>
                       </div>
                       @endif


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
$(document).ready(function(){
$('.btn-treino').each (function(){
   var btn = $(this);
   var treino = btn.text();
   var btnClass = 'btn-success';

   switch (treino) {
      case 'V':
         btnClass = 'btn-default';
         break;
      case 'D':
         btnClass = 'btn-info';
         break;
   }

   btn.text(treino);

   btn.removeClass('selected btn-info btn-success btn-default');
   btn.addClass(btnClass);
});
})
</script>

@endsection
