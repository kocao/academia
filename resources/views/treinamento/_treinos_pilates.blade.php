<table class="table">
  <tbody>
    <td colspan="6" class="head-treino text-center">
      Treino A
    </td>
    <tr>
      <td class="head-treino">Público</td>
      <td class="head-treino">Aquecimento</td>
      <td class="head-treino">Tempo</td>
      <td class="head-treino">Aparelho</td>
      <td class="head-treino">Exercício Específico</td>
      <td class="head-treino">Volta a Calma</td>
    </tr>
    @foreach($treinamento -> treinos as $key_treino => $treino)
    <tr>
      <td colspan="6" class="head-treino text-center">
        {{ Form::hidden("treino[$key_treino][sigla]", $treino -> sigla) }}
        {!! Form::text("treino[$key_treino][titulo_treino]", $treino -> titulo_treino, ['class' => 'text-center head-treino', 'readonly' => '']) !!}
      </td>
    </tr>
        {!! Form::hidden("treino[$key_treino][id_treino]", $treino -> id) !!}
            @foreach($treino -> atividades_pil as $key_atividade => $atividade)
              @include('treinamento._atividade_pil')
            @endforeach
      @endforeach
      </tbody>
   </table>
