@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Treinamento</div>
                    <div class="panel-body">
                        <?php $action = explode('@', Route::currentRouteAction())[1] ?>

                        @if($action == 'index')
                        <a href="{{ url('/treinamento/create') }}" class="btn btn-success btn-sm" title="Novo Treinamento">
                          <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                        {!! Form::open(['method' => 'GET', 'url' => '/treinamento', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}
                        @endif

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>Profissional</th>
                                        <th>Aluno</th>
                                        <th>Tipo Treino</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($treinamento as $item)
                                    <tr>
                                        <td>{{ $item->profissional -> name }}</td>
                                        <td>{{ $item->aluno -> name }}</td>
                                        <td>{{ $item::tipos_treino()[$item->tipo_treino] }}</td>
                                        <td>
                                            <a href="{{ url('/treinamento/' . $item->id) }}" title="Visualizar Treinamento"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            @if($action == 'index')
                                            <a href="{{ url('/treinamento/' . $item->id . '/edit') }}" title="Editar Treinamento"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/treinamento', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Deletar Treinamento',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                                @endif
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @if($action == 'index')
                            <div class="pagination-wrapper"> {!! $treinamento->appends(['search' => Request::get('search')])->render() !!}
                            </div>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
