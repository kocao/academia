@foreach($treinamento -> treinos as $key_treino => $treino)
  <table class="table">
     <tbody>
     <tr>
        <td colspan="4" class="head-treino text-center">
           {{ $treino -> titulo_treino }}
        </td>
     </tr>
     <tr>
        <td class="head-treino">Atividade</td>
        <td class="head-treino">Km</td>
        <td class="head-treino">Tempo</td>
        <td class="head-treino">Pace</td>
     </tr>
     @foreach($treino -> atividades as $key_atividade => $atividade)
     <tr>
        <td class="cell-treino">
           {!! $atividade -> descricao !!}
        </td>
        <td class="cell-treino">
           {!! $atividade -> distancia !!}
        </td>
        <td class="cell-treino">
           {!! $atividade -> tempo !!}
        </td>
        <td class="cell-treino">
           {!! $atividade -> pace !!}
        </td>
     </tr>
     @endforeach

     <tr>
        <td colspan="4" class="text-center cell-treino">
           Anotações
        </td>
     </tr>
     <tr>
        <td colspan="4" class="col-md-12 cell-treino">
           {{ $treino -> anotacoes }}
        </td>
     </tr>
     </tbody>
  </table>
@endforeach
