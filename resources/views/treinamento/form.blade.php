<div class="form-group {{ $errors->has('aluno_id') ? 'has-error' : ''}}">
    {!! Form::label('aluno_id', 'Aluno', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        @if(empty($treinamento -> id))
           {!! Form::select('aluno_id', $alunos); !!}
           {!! $errors->first('aluno_id', '<p class="help-block">:message</p>') !!}
        @else
          {{ $treinamento -> aluno -> name }}
        @endif
    </div>
</div>
{!! Form::hidden('id', (empty($treinamento) ? '' : $treinamento -> id)) !!}
<div class="form-group {{ $errors->has('tipo_treino') ? 'has-error' : ''}}">
    {!! Form::label('tipo_treino', 'Tipo de Treino', ['class' => 'col-md-4 control-label ']) !!}
    <div class="col-md-6">
        @if(empty($treinamento -> id))
           {!! Form::select('tipo_treino', $tipos_treino, null, ['class' => 'tipo_treino']); !!}
           {!! $errors->first('tipo_treino', '<p class="help-block">:message</p>') !!}
        @else
            {{ $tipos_treino[$treinamento -> tipo_treino ]}}
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('sublimiar1') ? 'has-error' : ''}}">
    {!! Form::label('sublimiar1', 'Sublimiar1', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('sublimiar1', null, ['class' => 'form-control']) !!}
        {!! $errors->first('sublimiar1', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('sublimiar2') ? 'has-error' : ''}}">
    {!! Form::label('sublimiar2', 'Sublimiar2', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('sublimiar2', null, ['class' => 'form-control']) !!}
        {!! $errors->first('sublimiar2', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('limiaraerobio') ? 'has-error' : ''}}">
    {!! Form::label('limiaraerobio', 'Limiaraerobio', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('limiaraerobio', null, ['class' => 'form-control']) !!}
        {!! $errors->first('limiaraerobio', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('supralimiar') ? 'has-error' : ''}}">
    {!! Form::label('supralimiar', 'Supralimiar', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('supralimiar', null, ['class' => 'form-control']) !!}
        {!! $errors->first('supralimiar', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('peso') ? 'has-error' : ''}}">
    {!! Form::label('peso', 'Peso', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('peso', null, ['class' => 'form-control']) !!}
        {!! $errors->first('peso', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('estatura') ? 'has-error' : ''}}">
    {!! Form::label('estatura', 'Estatura', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('estatura', null, ['class' => 'form-control']) !!}
        {!! $errors->first('estatura', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('objetivo') ? 'has-error' : ''}}">
    {!! Form::label('objetivo', 'Objetivo', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('objetivo', null, ['class' => 'form-control']) !!}
        {!! $errors->first('objetivo', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="treino">
   @include($view_treino)
</div>
@if($treinamento -> tipo_treino = null || $treinamento -> tipo_treino != 5)
<div id="periodos" class="table-responsive">
   <table class="table table-bordered" style="background-color:#c8cace;">
      <thead>
         <tr>
            <th></th>
            <th>S</th>
            <th>T</th>
            <th>Q</th>
            <th>Q</th>
            <th>S</th>
            <th>S</th>
            <th>D</th>
         </tr>
      </thead>

      <tbody>
         @for($i = 0; $i <=3; $i++)
            <tr>
               {{ Form::hidden('id_periodo[]', (empty($periodos) ? '' : $periodos[$i] -> id )) }}
               <td>{{ Form::text('semana[]', "S".($i+1), ['class' => 'bg-transparente sem-borda','readyonly' => '','size' => '2']) }}</td>
               <td>
                 {{ Form::hidden('segunda[]',(empty($periodos) ? 'V' : $periodos[$i] -> segunda),['class' => 'dia-treino'])}}
                  <button type="button" class="btn btn-default btn-xs btn-treino" data-toggle="modal" data-target="#myModal">{{empty($periodos) ? 'V' : $periodos[$i] -> segunda}}</button>
               </td>
               <td>
                  {{ Form::hidden('terca[]',(empty($periodos) ? 'V' : $periodos[$i] -> terca),['class' => 'dia-treino'])}}
                  <button type="button" class="btn btn-default btn-xs btn-treino" data-toggle="modal" data-target="#myModal">{{ empty($periodos) ? 'V' : $periodos[$i] -> terca }}</button>
               </td>
               <td>
                  {{ Form::hidden('quarta[]',(empty($periodos) ? 'V' : $periodos[$i] -> quarta),['class' => 'dia-treino'])}}
                  <button type="button" class="btn btn-default btn-xs btn-treino" data-toggle="modal" data-target="#myModal">{{empty($periodos) ? 'V' : $periodos[$i] -> quarta}}</button>
               </td>
               <td>
                  {{ Form::hidden('quinta[]',(empty($periodos) ? 'V' : $periodos[$i] -> quinta),['class' => 'dia-treino'])}}
                  <button type="button" class="btn btn-default btn-xs btn-treino" data-toggle="modal" data-target="#myModal">{{empty($periodos) ? 'V' : $periodos[$i] -> quinta}}</button>
               </td>
               <td>
                  {{ Form::hidden('sexta[]',(empty($periodos) ? 'V' : $periodos[$i] -> sexta),['class' => 'dia-treino'])}}
                  <button type="button" class="btn btn-default btn-xs btn-treino" data-toggle="modal" data-target="#myModal">{{ empty($periodos) ? 'V' : $periodos[$i] -> sexta }}</button>
               </td>
               <td>
                  {{ Form::hidden('sabado[]',(empty($periodos) ? 'V' : $periodos[$i] -> sabado),['class' => 'dia-treino'])}}
                  <button type="button" class="btn btn-default btn-xs btn-treino" data-toggle="modal" data-target="#myModal">{{empty($periodos) ? 'V' : $periodos[$i] -> sabado}}</button>
               </td>
               <td>
                  {{ Form::hidden('domingo[]',(empty($periodos) ? 'V' : $periodos[$i] -> domingo),['class' => 'dia-treino'])}}
                  <button type="button" class="btn btn-default btn-xs btn-treino" data-toggle="modal" data-target="#myModal">{{empty($periodos) ? 'V' : $periodos[$i] -> domingo}}</button>
               </td>
            </tr>
         @endfor
      </tbody>
   </table>
</div>
@endif

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Criar', ['class' => 'btn btn-primary']) !!}
    </div>
</div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Selecione o treino</h4>
      </div>
      <div id='modal_treino' class="modal-body text-center">
         @include('treinamento._modal_periodo')
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
      </div>
    </div>

  </div>
</div>
@section('js')
<script>
$(document).ready(function(){
$('.btn-treino').each (function(){
   var btn = $(this);
   var treino = btn.text();
   var btnClass = 'btn-success';

   switch (treino) {
      case 'V':
         btnClass = 'btn-default';
         break;
      case 'D':
         btnClass = 'btn-info';
         break;
   }

   btn.text(treino);

   btn.removeClass('selected btn-info btn-success btn-default');
   btn.addClass(btnClass);
});

})

$('.tipo_treino').change(function(){
   var token = $('meta[name="csrf-token"]').attr('content');
   var tipo_treino = $(this).val();
   if(tipo_treino == 5){
     $("#periodos").hide();
   } else {
     $("#periodos").show();
   }

   $.post('/view_treino', {_token: token, tipo_treino: $(this).val()}, function(data){
      $('.treino').html(data);
   })
   $.post('/modal_periodo', {_token: token, tipo_treino: $(this).val()}, function(data){
      $('#modal_treino').html(data);
   })
});

$('.btn-treino').click(function(){
   $(this).addClass('selected');
   $(this).parent().children('.dia-treino').addClass('selected');
})
// $('.select-treino').click(function(){
//    var treino = $(this).text();
//    var btn = $('.btn-treino.selected');
//    var hidden = $('.dia-treino.selected');
//    var btnClass = 'btn-success';
//
//    switch (treino) {
//       case 'Vazio':
//          btnClass = 'btn-default';
//          break;
//       case 'Descanso':
//          btnClass = 'btn-info';
//          break;
//    }
//
//    btn.text(treino);
//    hidden.val(treino);
//
//    btn.removeClass('selected btn-info btn-success btn-default');
//    btn.addClass(btnClass);
//
//    hidden.removeClass('selected');
// })
</script>
@endsection
