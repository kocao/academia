<div class="form-group {{ $errors->has('id_aluno') ? 'has-error' : ''}}">
    {!! Form::label('id_aluno', 'Aluno', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        @if(!empty($dieta))
        {{ $dieta -> aluno -> name}}
        @else
        {!! Form::select('id_aluno', $alunos); !!}
        {!! $errors->first('id_area_profissional', '<p class="help-block">:message</p>') !!}
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('data') ? 'has-error' : ''}}">
    {!! Form::label('data', 'Data', ['class' => 'col-md-4 control-label data']) !!}
    <div class="col-md-6">
        {!! Form::text('data', null, ['class' => 'form-control data', 'required'=>'', 'data-readonly' => '']) !!}
        {!! $errors->first('data', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('descricao') ? 'has-error' : ''}}">
    {!! Form::label('descricao', 'Descricao', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('descricao', null, ['class' => 'form-control', 'required' => '']) !!}
        {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
    </div>
</div>
      <div class="table table-responsive tabela">
      <table>
      <thead>
         <tr class="row headTable">
               <th class='col-md-2 headTable'>Refeições</th>
               <th class='col-md-1 headTable'>Segunda</th>
               <th class='col-md-1 headTable'>Terça</th>
               <th class='col-md-1 headTable'>Quarta</th>
               <th class='col-md-1 headTable'>Quinta</th>
               <th class='col-md-1 headTable'>Sexta</th>
               <th class='col-md-1 headTable'>Sábado</th>
               <th class='col-md-1 headTable'>Domingo</th>
               <th class='col-md-1 headTable'>Opção 1</th>
               <th class='col-md-1 headTable'>Opção 2</th>
               <th class='col-md-1 headTable'>Opção 3</th>
         </tr>
      </thead>
      <tbody>
      @foreach ($refeicoes as $key => $refeicao)
      {{ Form::hidden('id[]', (empty($dieta_refeicoes) ? null : $dieta_refeicoes[$key]['id']) )}}
      <tr class="row  headTable">
         <td class="col-md-1">{{ $refeicao }}</td>
         <td class="col-md-1">{{ Form::textarea('segunda[]', (empty($dieta_refeicoes) ? null : $dieta_refeicoes[$key]['segunda']), ['size' => '05x05']) }}</td>
         <td class="col-md-1">{{ Form::textarea('terca[]', (empty($dieta_refeicoes) ? null : $dieta_refeicoes[$key]['terca']), ['size' => '05x05']) }}</td>
         <td class="col-md-1">{{ Form::textarea('quarta[]', (empty($dieta_refeicoes) ? null : $dieta_refeicoes[$key]['quarta']), ['size' => '05x05']) }}</td>
         <td class="col-md-1">{{ Form::textarea('quinta[]', (empty($dieta_refeicoes) ? null : $dieta_refeicoes[$key]['quinta']), ['size' => '05x05']) }}</td>
         <td class="col-md-1">{{ Form::textarea('sexta[]', (empty($dieta_refeicoes) ? null : $dieta_refeicoes[$key]['sexta']), ['size' => '05x05']) }}</td>
         <td class="col-md-1">{{ Form::textarea('sabado[]', (empty($dieta_refeicoes) ? null : $dieta_refeicoes[$key]['sabado']), ['size' => '05x05']) }}</td>
         <td class="col-md-1">{{ Form::textarea('domingo[]', (empty($dieta_refeicoes) ? null : $dieta_refeicoes[$key]['domingo']), ['size' => '05x05']) }}</td>
         <td class="col-md-1">{{ Form::textarea('opcao1[]', (empty($dieta_refeicoes) ? null : $dieta_refeicoes[$key]['opcao1']), ['size' => '05x05']) }}</td>
         <td class="col-md-1">{{ Form::textarea('opcao2[]', (empty($dieta_refeicoes) ? null : $dieta_refeicoes[$key]['opcao2']), ['size' => '05x05']) }}</td>
         <td class="col-md-1">{{ Form::textarea('opcao3[]', (empty($dieta_refeicoes) ? null : $dieta_refeicoes[$key]['opcao3']), ['size' => '05x05']) }}</td>
      </tr>
      @endforeach
     </tbody>
     </table>
  </div>

<div class="form-group">
   <div class="col-md-offset-4 col-md-4">
      {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Criar', ['class' => 'btn btn-primary']) !!}
   </div>
</div>
@section('js')
<script>
$(document).ready(function(){
   $('#data').datepicker({
      dateFormat: 'dd/mm/yy',
      showOn: "button",
      buttonImage: "{{asset('images/calendar.png')}}",
      buttonImageOnly: true,
      buttonText: "Selecione a data"
   });
});
</script>
@stop
