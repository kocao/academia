<span class="cel_val">
@if(!empty($refeicao))
{!! substr($refeicao, 0, 1) !!}
<a href="#table-refeicao" onclick="$(this).parent().parent().children('.modal_cel').modal('show')">+</a>
@endif
</span>
<!-- Modal -->
<div class="modal fade modal_cel" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body text-center">
        {!! $refeicao !!}
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
</span>
