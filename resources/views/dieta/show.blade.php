@extends('adminlte::page')

@section('content')
<div class="row">
   <div class="col-md-9">
      <div class="panel panel-default">
         <div class="panel-heading">Visualizar Dieta</div>
         <div class="panel-body">
            <a href="{{ url('/dieta') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i></button></a>
            <br><br>
            <div class="form-group">
               {!! Form::label('aluno', 'Aluno', ['class' => 'col-md-4 control-label data', 'required'=>'']) !!}
               <div class="col-md-6">
                  <div class="form-control">
                     {!! $dieta -> aluno -> name !!}
                  </div>
               </div>
            </div>
            <div class="form-group">
               {!! Form::label('data', 'Data', ['class' => 'col-md-4 control-label data', 'required'=>'']) !!}
               <div class="col-md-6">
                  <div class="form-control">
                     {!! human_date($dieta -> data) !!}
                  </div>
               </div>
            </div>
            <div class="form-group {{ $errors->has('descricao') ? 'has-error' : ''}}">
               {!! Form::label('descricao', 'Descricao', ['class' => 'col-md-4 control-label']) !!}
               <div class="col-md-6">
                  <div class="form-control">
                     {!! $dieta -> descricao !!}
                  </div>
               </div>
            </div>
            <div class="row">
            </div>
            <br><br>
            <div id="table-refeicao" class="table-responsive">
               <table class="table">
                  <thead>
                     <tr>
                        <th class='col-md-1 headTable'>Refeições</th>
                        <th class='col-md-1 headTable'>S</th>
                        <th class='col-md-1 headTable'>T</th>
                        <th class='col-md-1 headTable'>Q</th>
                        <th class='col-md-1 headTable'>Q</th>
                        <th class='col-md-1 headTable'>S</th>
                        <th class='col-md-1 headTable'>S</th>
                        <th class='col-md-1 headTable'>D</th>
                        <th class='col-md-1 headTable'>O1</th>
                        <th class='col-md-1 headTable'>O2</th>
                        <th class='col-md-1 headTable'>O3</th>
                     </tr>
                  </thead>

                  <tbody>

                     @foreach ($dieta -> dieta_refeicoes as $key => $dieta_refeicao)
                     <tr>
                        <td class="headTable">{!! $refeicoes[$dieta_refeicao -> ordem_refeicao] !!}</td>
                        <td class="celula">
                          @include("dieta._refeicao", ['refeicao' => $dieta_refeicao -> segunda])
                        </td>
                        <td class="celula">@include("dieta._refeicao", ['refeicao' =>  $dieta_refeicao -> terca  ])</td>
                        <td class="celula">@include("dieta._refeicao", ['refeicao' =>  $dieta_refeicao -> quarta  ])</td>
                        <td class="celula">@include("dieta._refeicao", ['refeicao' =>  $dieta_refeicao -> quinta  ])</td>
                        <td class="celula">@include("dieta._refeicao", ['refeicao' =>  $dieta_refeicao -> sexta  ])</td>
                        <td class="celula">@include("dieta._refeicao", ['refeicao' =>  $dieta_refeicao -> sabado  ])</td>
                        <td class="celula">@include("dieta._refeicao", ['refeicao' =>  $dieta_refeicao -> domingo  ])</td>
                        <td class="celula">@include("dieta._refeicao", ['refeicao' =>  $dieta_refeicao -> opcao1  ])</td>
                        <td class="celula">@include("dieta._refeicao", ['refeicao' =>  $dieta_refeicao -> opcao2  ])</td>
                        <td class="celula">@include("dieta._refeicao", ['refeicao' =>  $dieta_refeicao -> opcao3  ])</td>
                     </tr>
                     @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   @endsection
