@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Histórico</div>
                    <div class="panel-body">
                       <div class="table-responsive">
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                @if($perfil != 'Aluno')
                                  <th>Aluno</th>
                                @endif
                                <th>Data</th>
                                <th>Descrição</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($historicos as $keh => $historico)
                              <tr>
                                @if($perfil != 'Aluno')
                                <td>{{ empty($historico -> aluno) ? "" : $historico -> aluno -> name }}</td>
                                @endif
                                <td>{{ human_date($historico -> data) }}</td>
                                @if($perfil == 'Aluno')
                                  <td> <a href="{{ url('/'. $historico -> link_aluno )}}">{{ $historico -> descricao }}</a></td>
                                @else
                                  <td> <a href="{{ url('/'. $historico -> link_profissional )}}">{{ $historico -> descricao }}</a></td>
                                @endif
                              </tr>
                              @endforeach

                            </tbody>
                          </table>

                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
@endsection
