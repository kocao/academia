@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Avaliacao Física</div>
                    <div class="panel-body">
                       <a href="{{ url('/avaliacao-fisica') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i></button></a>
                       <br><br>
                       <div class="row">
                          <div class="form-group {{ $errors->has('aluno_id') ? 'has-error' : ''}}">
                             {!! Form::label('aluno_id', 'Aluno', ['class' => 'col-md-4 control-label']) !!}
                             <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> aluno -> name }} </div> </div>
                          </div>
                       </div>
                       <div class="row">
                          <div class="form-group {{ $errors->has('data') ? 'has-error' : ''}}">
                            {!! Form::label('data', 'Data', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ human_date($avaliacaofisica -> data) }} </div> </div>
                          </div>
                       </div>
                       <br>
                       <br>
                       <div class="panel panel-info">
                         <div class="panel-heading">Composição Corporal</div>
                         <div class="panel-body">
                            <div class="form-group {{ $errors->has('peso') ? 'has-error' : ''}}">
                               {!! Form::label('peso', 'Peso Corporal (Kg)', ['class' => 'col-md-4 control-label']) !!}
                               <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> peso }} </div> </div>
                              <div class="form-group {{ $errors->has('estatura') ? 'has-error' : ''}}">
                               </div>
                               {!! Form::label('estatura', 'Estatura(cm)', ['class' => 'col-md-4 control-label']) !!}
                               <div class="col-md-6">
                                  <div class='form-control'>
                                     {{ $avaliacaofisica -> estatura }}
                                  </div>
                               </div>
                            </div><div class="form-group {{ $errors->has('gor_ideal') ? 'has-error' : ''}}">
                               {!! Form::label('gor_ideal', '% de gordura ideal', ['class' => 'col-md-4 control-label']) !!}
                               <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> gor_ideal }} </div> </div>
                            </div>
                            <div class="form-group {{ $errors->has('gor_atual') ? 'has-error' : ''}}">
                               {!! Form::label('gor_atual', '% de gordura atual', ['class' => 'col-md-4 control-label']) !!}
                               <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> gor_atual }} </div> </div>
                            </div>
                            <div class="form-group {{ $errors->has('peso_desejavel') ? 'has-error' : ''}}">
                               {!! Form::label('peso_desejavel', 'Peso desejável', ['class' => 'col-md-4 control-label']) !!}
                               <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> peso_desejavel }} </div> </div>
                            </div>
                            <div class="form-group {{ $errors->has('peso_magro') ? 'has-error' : ''}}">
                               {!! Form::label('peso_magro', 'Peso Magro(Kg)', ['class' => 'col-md-4 control-label']) !!}
                               <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> peso_magro }} </div> </div>
                            </div><div class="form-group {{ $errors->has('peso_gordo') ? 'has-error' : ''}}">
                               {!! Form::label('peso_gordo', 'Peso Gordo(Kg)', ['class' => 'col-md-4 control-label']) !!}
                               <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> peso_gordo }} </div> </div>
                            </div><div class="form-group {{ $errors->has('peso_residual') ? 'has-error' : ''}}">
                               {!! Form::label('peso_residual', 'Peso Residual(Kg)', ['class' => 'col-md-4 control-label']) !!}
                               <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> peso_residual }} </div> </div>
                            </div><div class="form-group {{ $errors->has('peso_muscular') ? 'has-error' : ''}}">
                               {!! Form::label('peso_muscular', 'Peso Muscular(Kg)', ['class' => 'col-md-4 control-label']) !!}
                               <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> peso_muscular }} </div> </div>
                            </div><div class="form-group {{ $errors->has('per_muscular') ? 'has-error' : ''}}">
                               {!! Form::label('per_muscular', '% Muscular', ['class' => 'col-md-4 control-label']) !!}
                               <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> per_muscular }} </div> </div>
                            </div>
                       </div>
                       </div>
                       <div class="panel panel-info">
                         <div class="panel-heading">Perímetro(cm)</div>
                         <div class="panel-body">
                            <div class="form-group {{ $errors->has('torax') ? 'has-error' : ''}}">
                            {!! Form::label('torax', 'Tórax', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> torax }} </div> </div>
                         </div><div class="form-group {{ $errors->has('cintura') ? 'has-error' : ''}}">
                            {!! Form::label('cintura', 'Cintura', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> cintura }} </div> </div>
                         </div><div class="form-group {{ $errors->has('abdomen') ? 'has-error' : ''}}">
                            {!! Form::label('abdomen', 'Abdômen', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> abdomen }} </div> </div>
                         </div><div class="form-group {{ $errors->has('quadril') ? 'has-error' : ''}}">
                            {!! Form::label('quadril', 'Quadril', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> quadril }} </div> </div>
                         </div><div class="form-group {{ $errors->has('coxa_direita') ? 'has-error' : ''}}">
                            {!! Form::label('coxa_direita', 'Coxa Direita', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> coxa_direita }} </div> </div>
                         </div><div class="form-group {{ $errors->has('coxa_esquerda') ? 'has-error' : ''}}">
                            {!! Form::label('coxa_esquerda', 'Coxa Esquerda', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> coxa_esquerda }} </div> </div>
                         </div><div class="form-group {{ $errors->has('pantu_direita') ? 'has-error' : ''}}">
                            {!! Form::label('pantu_direita', 'Panturrilha Direita', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> pantu_direita }} </div> </div>
                         </div><div class="form-group {{ $errors->has('pantu_esquerda') ? 'has-error' : ''}}">
                            {!! Form::label('pantu_esquerda', 'Panturrilha Esquerda', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> pantu_esquerda }} </div> </div>
                         </div><div class="form-group {{ $errors->has('ante_direito') ? 'has-error' : ''}}">
                            {!! Form::label('ante_direito', 'Antebraço Direito', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> ante_direito }} </div> </div>
                         </div><div class="form-group {{ $errors->has('ante_esquerdo') ? 'has-error' : ''}}">
                            {!! Form::label('ante_esquerdo', 'Antebraço Esquerdo', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> ante_esquerdo }} </div> </div>
                         </div><div class="form-group {{ $errors->has('braco_direito') ? 'has-error' : ''}}">
                            {!! Form::label('braco_direito', 'Braço Direito', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> braco_direito }} </div> </div>
                         </div><div class="form-group {{ $errors->has('braco_esquerdo') ? 'has-error' : ''}}">
                            {!! Form::label('braco_esquerdo', 'Braço Esquerdo', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> braco_esquerdo }} </div> </div>
                         </div>
                       </div>
                       </div>

                       <div class="panel panel-info">
                         <div class="panel-heading">Dobras</div>
                         <div class="panel-body">
                            <div class="form-group {{ $errors->has('subscapular') ? 'has-error' : ''}}">
                            {!! Form::label('subscapular', 'Subscapular', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> subscapular }} </div> </div>
                         </div><div class="form-group {{ $errors->has('tricipital') ? 'has-error' : ''}}">
                            {!! Form::label('tricipital', 'Tricipital', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> tricipital }} </div> </div>
                         </div><div class="form-group {{ $errors->has('peitoral') ? 'has-error' : ''}}">
                            {!! Form::label('peitoral', 'Peitoral', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> peitoral }} </div> </div>
                         </div><div class="form-group {{ $errors->has('axilar') ? 'has-error' : ''}}">
                            {!! Form::label('axilar', 'Axilar-média', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> axilar }} </div> </div>
                         </div><div class="form-group {{ $errors->has('supra_iliaca') ? 'has-error' : ''}}">
                            {!! Form::label('supra_iliaca', 'Supra Ilíaca', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> supra_iliaca }} </div> </div>
                         </div><div class="form-group {{ $errors->has('abdominal') ? 'has-error' : ''}}">
                            {!! Form::label('abdominal', 'Abdominal', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> abdominal }} </div> </div>
                         </div><div class="form-group {{ $errors->has('coxa') ? 'has-error' : ''}}">
                            {!! Form::label('coxa', 'Coxa', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6"> <div class='form-control'> {{ $avaliacaofisica -> coxa }} </div> </div>
                         </div>
                         </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
