<div class="form-group {{ $errors->has('aluno_id') ? 'has-error' : ''}}">
   {!! Form::label('aluno_id', 'Aluno', ['class' => 'col-md-4 control-label']) !!}
   <div class="col-md-6">
      @if(!empty($avaliacaofisica))
         {{ $avaliacaofisica -> aluno -> name}}
      @else
         {!! Form::select('aluno_id', $alunos, null, ['required' => '']); !!}
         {!! $errors->first('aluno_id', '<p class="help-block">:message</p>') !!}
      @endif
   </div>
</div>
<div class="form-group {{ $errors->has('data') ? 'has-error' : ''}}">
  {!! Form::label('data', 'Data', ['class' => 'col-md-4 control-label' ]) !!}
  <div class="col-md-6">
     {!! Form::text('data', null, ['class' => 'form-control bg-transparente', 'id' => 'data', 'required'=>'', 'data-readonly' => '']) !!}
     {!! $errors->first('data', '<p class="help-block">:message</p>') !!}
  </div>
</div>

<div class="panel panel-info">
  <div class="panel-heading">Composição Corporal</div>
  <div class="panel-body">
     <div class="form-group {{ $errors->has('peso') ? 'has-error' : ''}}">
        {!! Form::label('peso', 'Peso Corporal (Kg)', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
           {!! Form::text('peso', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
           {!! $errors->first('peso', '<p class="help-block">:message</p>') !!}
        </div>
       <div class="form-group {{ $errors->has('estatura') ? 'has-error' : ''}}">
        </div>
        {!! Form::label('estatura', 'Estatura(cm)', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
           {!! Form::text('estatura', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
           {!! $errors->first('estatura', '<p class="help-block">:message</p>') !!}
        </div>
     </div><div class="form-group {{ $errors->has('gor_ideal') ? 'has-error' : ''}}">
        {!! Form::label('gor_ideal', '% de gordura ideal', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
           {!! Form::text('gor_ideal', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
           {!! $errors->first('gor_ideal', '<p class="help-block">:message</p>') !!}
        </div>
     </div>
     <div class="form-group {{ $errors->has('gor_atual') ? 'has-error' : ''}}">
        {!! Form::label('gor_atual', '% de gordura atual', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
           {!! Form::text('gor_atual', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
           {!! $errors->first('gor_atual', '<p class="help-block">:message</p>') !!}
        </div>
     </div>
     <div class="form-group {{ $errors->has('peso_desejavel') ? 'has-error' : ''}}">
        {!! Form::label('peso_desejavel', 'Peso desejável', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
           {!! Form::text('peso_desejavel', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
           {!! $errors->first('peso_desejavel', '<p class="help-block">:message</p>') !!}
        </div>
     </div>
     <div class="form-group {{ $errors->has('peso_magro') ? 'has-error' : ''}}">
        {!! Form::label('peso_magro', 'Peso Magro(Kg)', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
           {!! Form::text('peso_magro', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
           {!! $errors->first('peso_magro', '<p class="help-block">:message</p>') !!}
        </div>
     </div><div class="form-group {{ $errors->has('peso_gordo') ? 'has-error' : ''}}">
        {!! Form::label('peso_gordo', 'Peso Gordo(Kg)', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
           {!! Form::text('peso_gordo', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
           {!! $errors->first('peso_gordo', '<p class="help-block">:message</p>') !!}
        </div>
     </div><div class="form-group {{ $errors->has('peso_residual') ? 'has-error' : ''}}">
        {!! Form::label('peso_residual', 'Peso Residual(Kg)', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
           {!! Form::text('peso_residual', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
           {!! $errors->first('peso_residual', '<p class="help-block">:message</p>') !!}
        </div>
     </div><div class="form-group {{ $errors->has('peso_muscular') ? 'has-error' : ''}}">
        {!! Form::label('peso_muscular', 'Peso Muscular(Kg)', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
           {!! Form::text('peso_muscular', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
           {!! $errors->first('peso_muscular', '<p class="help-block">:message</p>') !!}
        </div>
     </div><div class="form-group {{ $errors->has('per_muscular') ? 'has-error' : ''}}">
        {!! Form::label('per_muscular', '% Muscular', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
           {!! Form::text('per_muscular', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
           {!! $errors->first('per_muscular', '<p class="help-block">:message</p>') !!}
        </div>
     </div>
</div>
</div>
<div class="panel panel-info">
  <div class="panel-heading">Perímetro(cm)</div>
  <div class="panel-body">
     <div class="form-group {{ $errors->has('torax') ? 'has-error' : ''}}">
     {!! Form::label('torax', 'Tórax', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('torax', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('torax', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('cintura') ? 'has-error' : ''}}">
     {!! Form::label('cintura', 'Cintura', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('cintura', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('cintura', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('abdomen') ? 'has-error' : ''}}">
     {!! Form::label('abdomen', 'Abdômen', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('abdomen', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('abdomen', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('quadril') ? 'has-error' : ''}}">
     {!! Form::label('quadril', 'Quadril', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('quadril', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('quadril', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('coxa_direita') ? 'has-error' : ''}}">
     {!! Form::label('coxa_direita', 'Coxa Direita', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('coxa_direita', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('coxa_direita', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('coxa_esquerda') ? 'has-error' : ''}}">
     {!! Form::label('coxa_esquerda', 'Coxa Esquerda', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('coxa_esquerda', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('coxa_esquerda', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('pantu_direita') ? 'has-error' : ''}}">
     {!! Form::label('pantu_direita', 'Panturrilha Direita', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('pantu_direita', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('pantu_direita', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('pantu_esquerda') ? 'has-error' : ''}}">
     {!! Form::label('pantu_esquerda', 'Panturrilha Esquerda', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('pantu_esquerda', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('pantu_esquerda', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('ante_direito') ? 'has-error' : ''}}">
     {!! Form::label('ante_direito', 'Antebraço Direito', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('ante_direito', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('ante_direito', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('ante_esquerdo') ? 'has-error' : ''}}">
     {!! Form::label('ante_esquerdo', 'Antebraço Esquerdo', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('ante_esquerdo', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('ante_esquerdo', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('braco_direito') ? 'has-error' : ''}}">
     {!! Form::label('braco_direito', 'Braço Direito', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('braco_direito', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('braco_direito', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('braco_esquerdo') ? 'has-error' : ''}}">
     {!! Form::label('braco_esquerdo', 'Braço Esquerdo', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('braco_esquerdo', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('braco_esquerdo', '<p class="help-block">:message</p>') !!}
     </div>
  </div>
</div>
</div>

<div class="panel panel-info">
  <div class="panel-heading">Dobras</div>
  <div class="panel-body">
     <div class="form-group {{ $errors->has('subscapular') ? 'has-error' : ''}}">
     {!! Form::label('subscapular', 'Subscapular', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('subscapular', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('subscapular', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('tricipital') ? 'has-error' : ''}}">
     {!! Form::label('tricipital', 'Tricipital', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('tricipital', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('tricipital', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('peitoral') ? 'has-error' : ''}}">
     {!! Form::label('peitoral', 'Peitoral', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('peitoral', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('peitoral', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('axilar') ? 'has-error' : ''}}">
     {!! Form::label('axilar', 'Axilar-média', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('axilar', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('axilar', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('supra_iliaca') ? 'has-error' : ''}}">
     {!! Form::label('supra_iliaca', 'Supra Ilíaca', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('supra_iliaca', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('supra_iliaca', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('abdominal') ? 'has-error' : ''}}">
     {!! Form::label('abdominal', 'Abdominal', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('abdominal', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('abdominal', '<p class="help-block">:message</p>') !!}
     </div>
  </div><div class="form-group {{ $errors->has('coxa') ? 'has-error' : ''}}">
     {!! Form::label('coxa', 'Coxa', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
        {!! Form::text('coxa', null, ['class' => 'form-control', 'onkeyup' => 'numeros_mask($(this))']) !!}
        {!! $errors->first('coxa', '<p class="help-block">:message</p>') !!}
     </div>
  </div>
  </div>
</div>
<div class="form-group">
   <div class="col-md-offset-4 col-md-4">
      {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Criar', ['class' => 'btn btn-primary']) !!}
   </div>
</div>
@section('js')
<script type="text/javascript">
$(document).ready(function(){
   $('#data').datepicker({
      dateFormat: 'dd/mm/yy',
      showOn: "button",
      buttonImage: "{{asset('images/calendar.png')}}",
      buttonImageOnly: true,
      buttonText: "Selecione a data"
   });
});
</script>
@endsection
