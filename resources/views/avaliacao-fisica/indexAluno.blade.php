@extends('adminlte::page')
@section('content')
<div class="container">
   <div class="panel panel-default">
      <div class="panel-heading">Avaliações Físicas do Aluno</div>
      <div class="panel-body text-center">
         @if($avaliacaofisica -> count() > 0)
         <div class="row row-horizon">
            <div class="col-md-3 coluna">
               <div class="col-md-12 celula-avaliacao">Data</div>
               <div class="col-md-12 celula-avaliacao">Composição Corporal</div>
               <div class="col-md-12 celula-avaliacao">Peso Corporal(Kg)</div>
               <div class="col-md-12 celula-avaliacao">Estatura(cm)</div>
               <div class="col-md-12 celula-avaliacao">% de gordura ideal</div>
               <div class="col-md-12 celula-avaliacao">% de gordura atual</div>
               <div class="col-md-12 celula-avaliacao">Peso desejável(Kg)</div>
               <div class="col-md-12 celula-avaliacao">Peso magro(Kg)</div>
               <div class="col-md-12 celula-avaliacao">Peso gordo(Kg)</div>
               <div class="col-md-12 celula-avaliacao">Peso residual(Kg)</div>
               <div class="col-md-12 celula-avaliacao">Peso Muscular(Kg)</div>
               <div class="col-md-12 celula-avaliacao">% Muscular</div>

               <div class="col-md-12 celula-avaliacao">Perímetros(cm)</div>
               <div class="col-md-12 celula-avaliacao">Tórax</div>
               <div class="col-md-12 celula-avaliacao">Cintura</div>
               <div class="col-md-12 celula-avaliacao">Abdômem</div>
               <div class="col-md-12 celula-avaliacao">Quadril</div>
               <div class="col-md-12 celula-avaliacao">Coxa Direita</div>
               <div class="col-md-12 celula-avaliacao">Coxa Esquerda</div>
               <div class="col-md-12 celula-avaliacao">Panturrilha Direita</div>
               <div class="col-md-12 celula-avaliacao">Panturrilha Esquerda</div>
               <div class="col-md-12 celula-avaliacao">Antebraço Direito</div>
               <div class="col-md-12 celula-avaliacao">Antebraço Esquerdo</div>
               <div class="col-md-12 celula-avaliacao">Braço Direito</div>
               <div class="col-md-12 celula-avaliacao">Braço Esquerdo</div>

               <div class="col-md-12 celula-avaliacao">Dobras(mm)</div>
               <div class="col-md-12 celula-avaliacao">Subscapular</div>
               <div class="col-md-12 celula-avaliacao">Tricipital</div>
               <div class="col-md-12 celula-avaliacao">Peitoral</div>
               <div class="col-md-12 celula-avaliacao">Axilar-média</div>
               <div class="col-md-12 celula-avaliacao">Supra ilíaca</div>
               <div class="col-md-12 celula-avaliacao">Abdominal</div>
               <div class="col-md-12 celula-avaliacao">Coxa</div>
            </div>
            @foreach ($avaliacaofisica as $key => $avaliacao)
            <div class="col-md-2 coluna">
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['data'] == null ? '&nbsp' : human_date($avaliacao['data'])  !!}</div>
               <div class="col-md-12 celula-avaliacao">resultado</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['peso'] == null ? '&nbsp' : $avaliacao['peso']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['estatura'] == null ? '&nbsp' : $avaliacao['estatura']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['gor_ideal'] == null ? '&nbsp' : $avaliacao['gor_ideal']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['gor_atual'] == null ? '&nbsp' : $avaliacao['gor_atual']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['peso_desejavel'] == null ? '&nbsp' : $avaliacao['peso_desejavel']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['peso_magro'] == null ? '&nbsp' : $avaliacao['peso_magro']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['peso_gordo'] == null ? '&nbsp' : $avaliacao['peso_gordo']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['peso_residual'] == null ? '&nbsp' : $avaliacao['peso_residual']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['peso_muscular'] == null ? '&nbsp' : $avaliacao['peso_muscular']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['per_muscular'] == null ? '&nbsp' : $avaliacao['per_muscular']  !!}</div>

               <div class="col-md-12 celula-avaliacao">&nbsp</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['torax'] == null ? '&nbsp' : $avaliacao['torax']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['cintura'] == null ? '&nbsp' : $avaliacao['cintura']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['abdomen'] == null ? '&nbsp' : $avaliacao['abdomen']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['quadril'] == null ? '&nbsp' : $avaliacao['quadril']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['coxa_direita'] == null ? '&nbsp' : $avaliacao['coxa_direita']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['coxa_esquerda'] == null ? '&nbsp' : $avaliacao['coxa_esquerda']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['pantu_direita'] == null ? '&nbsp' : $avaliacao['pantu_direita']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['pantu_esquerda'] == null ? '&nbsp' : $avaliacao['pantu_esquerda']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['ante_direito'] == null ? '&nbsp' : $avaliacao['ante_direito']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['ante_esquerdo'] == null ? '&nbsp' : $avaliacao['ante_esquerdo']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['braco_direito'] == null ? '&nbsp' : $avaliacao['braco_direito']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['braco_esquerdo'] == null ? '&nbsp' : $avaliacao['braco_esquerdo']  !!}</div>

               <div class="col-md-12 celula-avaliacao">&nbsp</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['subscapular'] == null ? '&nbsp' : $avaliacao['subscapular']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['tricipital'] == null ? '&nbsp' : $avaliacao['tricipital']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['peitoral'] == null ? '&nbsp' : $avaliacao['peitoral']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['axilar'] == null ? '&nbsp' : $avaliacao['axilar']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['supra_iliaca'] == null ? '&nbsp' : $avaliacao['supra_iliaca'] !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['abdominal'] == null ? '&nbsp' : $avaliacao['abdominal']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $avaliacao['coxa'] == null ? '&nbsp' : $avaliacao['coxa']  !!}</div>
            </div>
            @if($key <> 0)
            <div class="col-md-2 coluna">
               <div class="col-md-12 celula-avaliacao">&nbsp</div>
               <div class="col-md-12 celula-avaliacao">Diferença</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['peso'] == null ? '&nbsp' : $diferenca[$key]['peso']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['estatura'] == null ? '&nbsp' : $diferenca[$key]['estatura']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['gor_ideal'] == null ? '&nbsp' : $diferenca[$key]['gor_ideal']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['gor_atual'] == null ? '&nbsp' : $diferenca[$key]['gor_atual']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['peso_desejavel'] == null ? '&nbsp' : $diferenca[$key]['peso_desejavel']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['peso_magro'] == null ? '&nbsp' : $diferenca[$key]['peso_magro']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['peso_gordo'] == null ? '&nbsp' : $diferenca[$key]['peso_gordo']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['peso_residual'] == null ? '&nbsp' : $diferenca[$key]['peso_residual']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['peso_muscular'] == null ? '&nbsp' : $diferenca[$key]['peso_muscular']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['per_muscular'] == null ? '&nbsp' : $diferenca[$key]['per_muscular']  !!}</div>

               <div class="col-md-12 celula-avaliacao">&nbsp</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['torax'] == null ? '&nbsp' : $diferenca[$key]['torax']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['cintura'] == null ? '&nbsp' : $diferenca[$key]['cintura']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['abdomen'] == null ? '&nbsp' : $diferenca[$key]['abdomen']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['quadril'] == null ? '&nbsp' : $diferenca[$key]['quadril']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['coxa_direita'] == null ? '&nbsp' : $diferenca[$key]['coxa_direita']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['coxa_esquerda'] == null ? '&nbsp' : $diferenca[$key]['coxa_esquerda']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['pantu_direita'] == null ? '&nbsp' : $diferenca[$key]['pantu_direita']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['pantu_esquerda'] == null ? '&nbsp' : $diferenca[$key]['pantu_esquerda']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['ante_direito'] == null ? '&nbsp' : $diferenca[$key]['ante_direito']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['ante_esquerdo'] == null ? '&nbsp' : $diferenca[$key]['ante_esquerdo']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['braco_direito'] == null ? '&nbsp' : $diferenca[$key]['braco_direito']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['braco_esquerdo'] == null ? '&nbsp' : $diferenca[$key]['braco_esquerdo']  !!}</div>

               <div class="col-md-12 celula-avaliacao">&nbsp</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['subscapular'] == null ? '&nbsp' : $diferenca[$key]['subscapular']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['tricipital'] == null ? '&nbsp' : $diferenca[$key]['tricipital']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['peitoral'] == null ? '&nbsp' : $diferenca[$key]['peitoral']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['axilar'] == null ? '&nbsp' : $diferenca[$key]['axilar']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['supra_iliaca'] == null ? '&nbsp' : $diferenca[$key]['supra_iliaca'] !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['abdominal'] == null ? '&nbsp' : $diferenca[$key]['abdominal']  !!}</div>
               <div class="col-md-12 celula-avaliacao">{!! $diferenca[$key]['coxa'] == null ? '&nbsp' : $diferenca[$key]['coxa']  !!}</div>
            </div>
            @endif
         @endforeach
         </div>
         <div class=""> {{ $avaliacaofisica->links() }} </div>
         @else
         <h1>Ainda não foi cadastrada avaliação física para você.</h1>
         @endif
      </div>
   </div>
</div>
@endsection
