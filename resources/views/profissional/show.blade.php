@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Visualizar Profissional</div>
                    <div class="panel-body">

                        <a href="{{ url('/admin/profissional') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                        <a href="{{ url('/admin/profissional/' . $profissional->id . '/edit') }}" title="Editar Profissional"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/profissional', $profissional->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Deletar', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Deletar Profissional',
                                    'onclick'=>'return confirm("Tem certeza?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                           <table class="table table-borderless">
                              <tbody>
                                 <tr>
                                    <th> Nome </th>
                                    <td> {{ $profissional -> usuario -> name }} </td>
                                 </tr>
                                 <tr>
                                    <th> Email </th>
                                    <td> {{ $profissional -> usuario -> email }} </td>
                                 </tr>
                                 <tr>
                                    <th> Telefone </th>
                                    <td>{{ $profissional -> telefone }}</td>
                                 </tr>
                                 <tr>
                                    <th> Whatsapp </th>
                                    <td>{{ $profissional -> whatsap }}</td>
                                 </tr>
                                 <tr>
                                    <th> Área Profissional </th>
                                    <td> {{ $profissional-> area_profissional -> descr_area }} </td>
                                 </tr>
                                 <tr>
                                    <th> Registro Profissional </th>
                                    <td> {{ $profissional->registro_profissional }} </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
