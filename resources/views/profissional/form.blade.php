<div class="col-md-6">
<div class="form-group {{ $errors->has('id_area_profissional') ? 'has-error' : ''}}">
    {!! Form::label('id_area_profissional', 'Área Profissional', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('id_area_profissional', $areas_profissionais); !!}
        {!! $errors->first('id_area_profissional', '<p class="help-block">:message</p>') !!}
    </div>
</div>
 <div class="form-group {{ $errors->has('registro_profissional') ? 'has-error' : ''}}">
    {!! Form::label('nome', 'Nome', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nome', ( isset($profissional) == false ? null : $profissional -> usuario -> name), ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group {{ $errors->has('registro_profissional') ? 'has-error' : ''}}">
    {!! Form::label('registro_profissional', 'Registro Profissional', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('registro_profissional', null, ['class' => 'form-control']) !!}
        {!! $errors->first('registro_profissional', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('email', ( isset($profissional) == false ? null : $profissional -> usuario -> email ), ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('telefone') ? 'has-error' : ''}}">
    {!! Form::label('telefone', 'Telefone', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
        {!! $errors->first('telefone', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('whatsap') ? 'has-error' : ''}}">
    {!! Form::label('whatsap', 'Whatsap', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('whatsap', null, ['class' => 'form-control']) !!}
        {!! $errors->first('whatsap', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if (isset($profissional) == false)
<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    {!! Form::label('password', 'senha', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password('password', ( isset($profissional) == false ? null : null), ['class' => 'form-control']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Criar', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
</div>
