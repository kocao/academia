@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Profissional</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/profissional/create') }}" class="btn btn-success btn-sm" title="Add New Profissional">
                            <i class="fa fa-plus" aria-hidden="true"></i> Novo Profissional
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/profissional', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Área Profissional</th><th>Registro Profissional</th><th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($profissional as $item)
                                    <tr>
                                        <td>{{ $item -> usuario -> name }}</td>
                                        <td>{{ $item -> area_profissional -> descr_area }}</td>
                                        <td>{{ $item->registro_profissional }}</td>
                                        <td>
                                            <a href="{{ url('/admin/profissional/' . $item->id) }}" title="View Profissional"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> Visualizar</button></a>
                                            <a href="{{ url('/admin/profissional/' . $item->id . '/edit') }}" title="Editar Profissional"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/profissional', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Deletar', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete Profissional',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $profissional->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
