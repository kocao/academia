@extends('adminlte::page')

@section('content')
<div class="container">
   <div class="row">

      <div class="col-md-9">
         <div class="panel panel-default">
            <div class="panel-heading">Exame</div>
            <div class="panel-body">
              <a href="{{ url('/exame?id_user='.$exame->id_user) }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i></button></a>
              <br />
              <br />


               <div class="table-responsive">
                  <table class="table table-borderless">
                     <tbody>
                        <tr>
                           <th> Medico </th>
                           <td> {{ $exame->medico }} </td>
                        </tr>
                        <tr>
                           <th> CRM </th>
                           <td> {{ $exame->crm }} </td>
                        </tr>
                        <tr>
                           <th> Aluno </th>
                           <td> {{ $exame-> aluno -> name }} </td>
                        </tr>
                        <tr>
                           <th> Diagnostico </th>
                           <td> {{ $exame-> aluno -> diagnostico }} </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
               @if(!empty($exame -> file))
               <div class="form-group">
                   {!! Form::label('file', 'Imagem do Exame', ['class' => 'col-md-4 control-label']) !!}
                   <div class="col-md-6">
                     {!! Html::image($exame -> file -> url_arquivo, 'exame', array('class' => 'thumb img img-responsive')) !!}
                   </div>
               </div>
               @endif
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
