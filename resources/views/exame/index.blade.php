@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Exame</div>
                    <div class="panel-body">
                       @if($perfil == "Aluno")
                        <a href="{{ url('/exame/create') }}" class="btn btn-success btn-sm" title="Novo Exame">
                            <i class="fa fa-plus" aria-hidden="true"></i></a>
                        @endif

                        {!! Form::open(['method' => 'GET', 'url' => '/exame', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>Médico</th>
                                        <th>
                                          {{ $perfil == "Aluno" ? "Ações" : "Visualizar" }}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($exames as $item)
                                    <tr>
                                        <td>{{ $item->medico }}</td>
                                        <td>
                                            <a href="{{ url('/exame/' . $item->id) }}" title="Visualizar Exame"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            @if($perfil == "Aluno")
                                            <a href="{{ url('/exame/' . $item->id . '/edit') }}" title="Editar Exame"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/exame', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Deletar Exame',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $exames->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
