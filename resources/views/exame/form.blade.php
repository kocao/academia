<div class="form-group {{ $errors->has('medico') ? 'has-error' : ''}}">
    {!! Form::label('medico', 'Medico', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('medico', null, ['class' => 'form-control', 'required' => '']) !!}
        {!! $errors->first('medico', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('crm') ? 'has-error' : ''}}">
    {!! Form::label('crm', 'Crm', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('crm', null, ['class' => 'form-control','required']) !!}
        {!! $errors->first('crm', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('data') ? 'has-error' : ''}}">
    {!! Form::label('data', 'Data', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('data', null, ['class' => 'form-control', 'required' => '', 'data-readonly' => '', 'id' => 'data']) !!}
        {!! $errors->first('data', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('diagnostico') ? 'has-error' : ''}}">
    {!! Form::label('diagnostico', 'Diagnostico', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('diagnostico', null, ['class' => 'form-control', 'required' => '']) !!}
        {!! $errors->first('diagnostico', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="imagem_exame">
   @if(!empty($arquivo))
   <div class="form-group">
       {!! Form::label('file', 'Imagem do Exame', ['class' => 'col-md-4 control-label']) !!}
       <div class="col-md-6">
         {!! Html::image($arquivo -> url_arquivo, 'exame', array('class' => 'thumb image image-responsive')) !!}
            <button class="btn btn-sm btn-danger" type="button" name="button" id="exclui_imagem" onclick="deleteImage({{ $arquivo -> id}});">
               <i class="fa fa-trash-o" aria-hidden="true"></i>
            </button>
       </div>

   </div>
   @else
   <div class="form-group">
       {!! Form::label('file', 'Imagem do Exame', ['class' => 'col-md-4 control-label', 'required' => '']) !!}
       <div class="col-md-6">
           {!! Form::file('file') !!}
       </div>
   </div>
   @endif
</div>
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Criar', ['class' => 'btn btn-primary']) !!}
    </div>
</div>


@section('js')
<script>
var token = $('meta[name="csrf-token"]').attr('content');
function deleteImage(id){
   $.post('/file/delete', {_token: token, id: id }, function(data){
      var fileInput = ' <label for="file" class="col-md-4 control-label">Imagem do Exame</label><div class="col-md-6"><input name="file" id="file" type="file"></div>';
      $('.imagem_exame').html(fileInput);
   });
}

$(document).ready(function(){
   $('#data').datepicker({
      dateFormat: 'dd/mm/yy',
      showOn: "button",
      buttonImage: "{{asset('images/calendar.png')}}",
      buttonImageOnly: true,
      buttonText: "Selecione a data"
   });
});

// $('#exclui_imagem').click(function(){
//    alert('clicou');
// });
</script>
@stop
