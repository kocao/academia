@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                @if(Session::has('flash_message'))
                  <div class="panel panel-success">
                      <div class="panel-body bg-success text-success text-center">
                        {{ Session::get('flash_message') }}
                      </div>
                  </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Agendar Consulta</div>
                    <div class="panel-body text-center">
                       <div class="row">
                          <div class="col-md-12 coluna-agenda">
                             {!! Form::open(['url' => '/agendamento', 'class' => 'form-horizontal', 'files' => true]) !!}
                             <div class="row">

                             {!! Form::text('data', human_date($data), ['id' => 'data','class' => 'data', 'required' => '', 'data-readonly' => '']) !!}
                             </div>
                             <div id="horarios_view" class="text-center">
                                <div class="col-md-12">
                                @include('agendamento._horarios_view')
                                </div>
                             </div>
                             {!! Form::hidden('horario_disponivel_id', null, ['id' => 'hora']) !!}
                          </div>
                       </div>
                       {!! Form::submit('Agendar', ['class' => 'btn btn-info']) !!}

                       {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Selecione o aluno</h4>
          </div>
          <div class="modal-body text-center">
            {{ Form::select('aluno_selecionado', [null=>'Selecione o Aluno'] + $alunos, null,['id' => 'aluno_selecionado'])}}
            <br><br>
            <div class="row">
              {{ Form::text('descricao_modal', null, ['placeholder' => 'Descrição', 'id' => 'descricao_modal'])}}
            </div>
          </div>
          <div class="modal-footer">
            <button id='btn-select-aluno' type="button" class="btn btn-success pull-left" data-dismiss="modal">Selecionar</button>
            <button onclick="$('.clicked').removeClass('clicked')"type="button" class="btn btn-danger pull-right" data-dismiss="modal">Fechar</button>
          </div>
        </div>

      </div>
    </div>

@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
$(document).ready(function(){
   $('#data').datepicker({
      dateFormat: 'dd/mm/yy',
      showOn: "button",
      buttonImage: "{{asset('images/calendar.png')}}",
      buttonImageOnly: true,
      buttonText: "Selecione a data"
   });
});
$('#data').change(function(){
   var token = $('meta[name="csrf-token"]').attr('content');
        window.location.href = "/agendamento/create?data=" + $(this).val();
        //  $.post('/horarios_view',{ _token: token, data: $(this).val() }, function(data){
        //    $('#horarios_view').html(data);
        //  });
});

$('.btn-horario').click(function(){
   $(this).addClass('clicked');
})

$('#btn-select-aluno').click(function(){
   var clicked = $('.clicked');
   var aluno_selecionado = $('#aluno_selecionado').val();
   var descricao_modal = $('#descricao_modal').val();
   clicked.parent().children('#aluno_id').val(aluno_selecionado);
   clicked.parent().children('.descricao').val(descricao_modal);
   clicked.text($('#aluno_selecionado option:selected').text());
   clicked.parent().parent().children('.descricao').html(descricao_modal);

   if(aluno_selecionado == ''){
     clicked.removeClass('btn-success');
     clicked.addClass('btn-default');
   } else {
     clicked.removeClass('btn-default');
     clicked.addClass('btn-success');
   }
   clicked.removeClass('clicked');

})

$('.linha-agenda').click(function(){
   $('.linha-agenda').removeClass('selected');
   $(this).addClass('selected');
   $('#hora').val($(this).attr('id'));
});
</script>
@stop
