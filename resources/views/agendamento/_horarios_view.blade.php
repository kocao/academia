@foreach($horarios as $key => $horario)
<div class="row">
  <div class="row" style="font-size: 30px;">
    {{ $horario -> hora }}
  </div>
  <div  class='descricao row' style='font-size: 20px'>
    {{ $horario -> descricao }}
  </div>
  <div class="row " style="font-size: 30px;">
    {{ Form::hidden('aluno_id[]', $horario -> aluno_id , ['id' => 'aluno_id']) }}
    {{ Form::hidden('horario_id[]', $horario -> id ) }}
    {{ Form::hidden('descricao[]', $horario -> descricao , ['class' => 'descricao']) }}
    <button type="button" class="btn {{ empty($horario -> aluno_id) ? 'btn-default' : 'btn-success'}} btn-sm btn-horario"
      data-toggle="modal" data-target="#myModal">{{ empty($horario -> aluno_id) ? 'Selecione o Aluno' : $horario -> nome }}</button>
  </div>
</div>
<hr>
@endforeach
