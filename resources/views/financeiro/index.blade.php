@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Financeiro</div>
                    <div class="panel-body">
                       {!! Form::open(['url' => '/financeiro', 'class' => 'form-horizontal submit']) !!}
                       <div class="form-group">
                          {!! Form::label('mes', 'Mês', ['class' => 'col-md-4 control-label']) !!}
                          <div class="col-md-6">
                             {{ Form::select('mes', meses_select(), $mes, ['class' => 'form-control mes_ano', 'id' => 'mes'])}}
                          </div>
                       </div>
                       <div class="form-group">
                          {!! Form::label('ano', 'Ano', ['class' => 'col-md-4 control-label']) !!}
                          <div class="col-md-6">
                             {{ Form::select('ano', anos_select(), $ano, ['class' => 'form-control mes_ano', 'id' => 'ano'])}}
                          </div>
                       </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="pagar text-center" colspan="6">Contas a receber</th>
                                        <!-- <th class="receber text-center" colspan="5">Recebimento</th> -->
                                    </tr>
                                    <tr>
                                       <th class='pagar'>#</th>
                                       <th class="pagar">Cliente</th>
                                       <th class="pagar">Vcto.</th>
                                       <th class="pagar">Valor</th>
                                       <th class="pagar">Recebeu</th>
                                       <th class="pagar">Excluir</th>
                                       <!-- <th class="receber">Data</th>
                                       <th class="receber">Valor</th>
                                       <th class="receber">Desconto</th>
                                       <th class="receber">Juros</th>
                                       <th class="receber"></th>
                                    </tr> -->
                                </thead>
                                <tbody class="body-table-pagar">
                                   @foreach ($contas as $key => $conta)
                                       @if($conta -> recebeu == 1)
                                         <tr class="{{'pagar_'.$conta -> id}}">
                                            <td class='order pagar'></td>
                                            <td class="pagar">{{ empty($conta -> aluno) ? '' : $conta -> aluno -> name }}</td>
                                            <td class="pagar">{{ human_date($conta -> dt_vcto) }}</td>
                                            <td class="pagar">{{ $conta -> vlr_vencer }}</td>
                                            <td class="pagar"><input type="checkbox" name="teste" value="1" checked='' onclick="return false;"/></td>
                                            <td class="pagar"><button type="button" class="btn btn-danger" disabled><i class="fa fa-trash"></i></button></td>
                                         </tr>
                                       @else
                                          @include('financeiro/_conta_financeiro')
                                       @endif
                                   @endforeach
                                </tbody>
                                <tfoot>
                                   <tr>
                                      <td colspan="6" class="pagar"><strong>Total a receber:</strong>
                                         <span id="total_pagar"><span>
                                      </td>
                                      <!-- <td colspan="5" class="receber">
                                        <strong>Total a recebido:</strong><span id="total_receber"></span>
                                      </td> -->
                                   </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="receber text-center" colspan="4">Recebimento</th>
                                    </tr>
                                    <tr>
                                       <th class='receber'>#</th>
                                       <th class="receber">Profissional</th>
                                       <th class="receber">Data</th>
                                       <th class="receber">Valor</th>
                                    </tr>
                                </thead>
                                <tbody class="body-table-receber">
                                   @foreach ($contas as $key => $conta)
                                     @if($conta -> recebeu == 1)
                                       <tr>
                                         <td class='order receber'></td>
                                         <td class="receber">{{ empty($conta -> profissional) ? '' : $conta -> profissional -> name }}</td>
                                         <td class="receber">{{ human_date($conta -> dt_pgto) }}</td>
                                         <td class="receber">{{ $conta -> vlr_pago }}</td>
                                       </tr>
                                     @else
                                         @include('financeiro/_linha_receber')
                                     @endif
                                   @endforeach
                                </tbody>
                                <tfoot>
                                   <tr>
                                      <td colspan="5" class="receber">
                                        <strong>Total a recebido:</strong><span id="total_receber"></span>
                                      </td>
                                   </tr>
                                </tfoot>
                            </table>
                        </div>

                           <button type="button" class="btn btn-success add-conta" onclick="window.add_conta()"><i class="fa fa-plus"></i></button>


                        {!! Form::submit('Salvar', ['class' => 'btn btn-info pull-right']) !!}

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
   $(window).load(function(){
      soma_total();
      sequence();
      $('.data_input').datepicker({
         dateFormat: 'dd/mm/yy',
         buttonText: "Selecione a data"
      });
   });
   // $('.vlr_soma').change(function(){
   //    soma_total();

   $('.submit').on('submit',function(event){
       event.preventDefault();
       var salvar = true;
       $('.recebido').each(function(){
         var row = $(this).parent().index();
         if($(this).val() == '1'){
           var aluno_id = $('.body-table-pagar').children().eq(row).find('.aluno_id').val();
           var profissional_id = $('.body-table-receber').children().eq(row).find('.profissional_id').val();
           if(aluno_id == '' || profissional_id == '0'){
             alert('É obrigatório selecionar aluno e profissional.');
             salvar =  false;
           }
         }
       })
       if(salvar){
          $(this).unbind('submit').submit();
       }
   });
   function form_validate(evt){
     $('.recebido').each(function(){
       var row = $(this).parent().index();
       if($(this).val() == '1'){
         var aluno_id = $('.body-table-pagar').children().eq(row).find('.aluno_id').val();
         var profissional_id = $('.body-table-receber').children().eq(row).find('.profissional_id').val();
         if(aluno_id == '' || profissional_id == '0'){
           alert('É obrigatório selecionar aluno e profissional.')
         }
       }
     })

    //  var row = element.parent().parent().index();
    //  $('.body-table-pagar').children().eq(row).remove();
    //  $('.body-table-receber').children().eq(row).remove();

   }

   function profissional_select(element){
     var val = element.val();
     element.parent().parent().find('.profissional_id').val(val);
   }
   // });
   $('.mes_ano').change(function(){
      var mes = $('#mes').val();
      var ano = $('#ano').val();

      window.location.href = "financeiro?ano=" + ano +"&mes=" + mes;
   });
   function sequence(){
     $('.order.pagar').each(function(index, value){
       $(this).html(index + 1);
     })
     $('.order.receber').each(function(index, value){
       $(this).html(index + 1);
     })
   }

   function add_conta(){
      var token = $('meta[name="csrf-token"]').attr('content');
      $.post('add_conta', {_token: token}, function(data){
         $('.body-table-pagar').append(data);
      }).done(function(){
         $('.data_input').datepicker({
            dateFormat: 'dd/mm/yy',
            buttonText: "Selecione a data"
         })
      });
      $.post('add_receber', {_token: token}, function(data){
         $('.body-table-receber').append(data);
      }).done(function(){
         $('.data_input').datepicker({
            dateFormat: 'dd/mm/yy',
            buttonText: "Selecione a data"
         });
         sequence();
      });

   }

   function soma_total(){
      var total_pagar = 0;
      var total_receber = 0;
      $(".vlr_vencer").each(function(){
         if($(this).val() != ''){
            total_pagar += parseInt($(this).val());
         }
      });
      $('#total_pagar').html(total_pagar);

      $(".vlr_pago").each(function(){
         if($(this).val() != ''){
            total_receber += parseInt($(this).val());
         }
      });
      $('#total_receber').html(total_receber);
   }

   function check_receber(element){
      var row = element.parent().parent().index();
      if(element.is(':checked') == true){
        $('.body-table-receber').children().eq(row).show();
        element.parent().parent().find('.recebido').val('1');
      } else {
        $('.body-table-receber').children().eq(row).hide();
        $('.body-table-receber').children().eq(row).find('.input').val('');
        element.parent().parent().find('.recebido').val('0');
      }
   }

   function excluir(element){
      var token = $('meta[name="csrf-token"]').attr('content');
      var financeiro_id = element.parent().parent().find('.financeiro_id').val();
      var row = element.parent().parent().index();
      $('.body-table-pagar').children().eq(row).remove();
      $('.body-table-receber').children().eq(row).remove();

      if(financeiro_id > 0){
        $.ajax({
            url: '/financeiro/' + financeiro_id,
            data: {"_token": token },
            type: 'DELETE',
            success: function(result) {
                // Do something with the result
            }
        });
      }

      sequence();
   }

</script>
@endsection
