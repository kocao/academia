<tr style='display:none;'>
  <td class='order receber'></td>
  {{ Form::hidden('profissional_id[]', '0', ['class' => 'profissional_id'] )}}

  <td class="receber">{{ Form::select('profissional_select[]', [null => ''] + $profissionais, $conta -> profissional_id,
    ['class' => 'input sem-borda bg-transparente profissional_select', 'default' => '', 'onchange' => 'profissional_select($(this))']) }}</td>
  <td class="receber">{{ Form::text('dt_pgto[]', human_date($conta -> dt_pgto), ['class' => 'input sem-borda bg-transparente data_input','readonly' => '']) }}</td>
  <td class="receber">{{ Form::text('vlr_pago[]', $conta -> vlr_pago, ['class' => 'input sem-borda bg-transparente vlr_pago vlr_soma', 'onchange' => 'soma_total()', 'onkeyup' => 'moeda($(this))']) }}</td>
</tr>
