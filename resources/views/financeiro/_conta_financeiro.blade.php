<tr>
   <td class='order pagar'></td>
   {{ Form::hidden('recebeu[]', '0', ['class' => 'recebido'] )}}
   {{ Form::hidden('financeiro_id[]', $conta -> id, ['class' => 'financeiro_id'] )}}
   <td class="pagar">{{ Form::select('aluno_id[]', $alunos, $conta -> aluno_id, ['class' => 'sem-borda bg-transparente aluno_id']) }}</td>
   <td class="pagar">{{ Form::text('dt_vcto[]', human_date($conta -> dt_vcto), ['class' => 'sem-borda bg-transparente data_input','readonly' => '']) }}</td>
   <td class="pagar">{{ Form::text('vlr_vencer[]', $conta -> vlr_vencer, ['class' => 'sem-borda bg-transparente vlr_vencer vlr_soma', 'onchange' => 'soma_total()', 'onkeyup' => 'moeda($(this))']) }}</td>
   <td class="pagar">{{ Form::checkbox('check_recebeu[]',1,null, ['class' => 'sem-borda bg-transparente', 'onchange' => 'check_receber($(this))'])  }}</td>
   <td class="pagar"><button onclick="window.excluir($(this))" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
</tr>
