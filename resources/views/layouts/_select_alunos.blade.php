<div class="form-group {{ $errors->has('aluno_id') ? 'has-error' : ''}}">
    {!! Form::label('aluno_id', 'Aluno', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('aluno_id', $alunos); !!}
        {!! $errors->first('aluno_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
