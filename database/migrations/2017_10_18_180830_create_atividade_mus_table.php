<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAtividadeMusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atividades_mus', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('treino_id')->unsigned();
            $table->timestamps();
            $table -> text('exercicio')->nullable();
            $table -> text('serie')->nullable();
            $table -> text('repeticao')->nullable();
            $table -> text('carga')->nullable();
            $table -> text('ajustes')->nullable();
            $table -> text('pausa')->nullable();

            $table -> foreign('treino_id') -> references('id') -> on('treinos') -> onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('atividades_mus');
    }
}
