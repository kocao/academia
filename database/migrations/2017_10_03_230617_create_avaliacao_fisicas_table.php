<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAvaliacaoFisicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacoes_fisicas', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('aluno_id')->unsigned();
            $table->date('data');
            $table->integer('profissional_id')->unsigned();
            $table->string('peso') -> nullable();
            $table->string('estatura') -> nullable();
            $table->string('gor_ideal') -> nullable();
            $table->string('gor_atual') -> nullable();
            $table->string('peso_magro') -> nullable();
            $table->string('peso_gordo') -> nullable();
            $table->string('peso_desejavel') -> nullable();
            $table->string('peso_residual') -> nullable();
            $table->string('peso_muscular') -> nullable();
            $table->string('per_muscular') -> nullable();
            $table->string('torax') -> nullable();
            $table->string('cintura') -> nullable();
            $table->string('abdomen') -> nullable();
            $table->string('quadril') -> nullable();
            $table->string('coxa_direita') -> nullable();
            $table->string('coxa_esquerda') -> nullable();
            $table->string('pantu_direita') -> nullable();
            $table->string('pantu_esquerda') -> nullable();
            $table->string('ante_direito') -> nullable();
            $table->string('ante_esquerdo') -> nullable();
            $table->string('braco_direito') -> nullable();
            $table->string('braco_esquerdo') -> nullable();
            $table->string('subscapular') -> nullable();
            $table->string('tricipital') -> nullable();
            $table->string('peitoral') -> nullable();
            $table->string('axilar') -> nullable();
            $table->string('supra_iliaca') -> nullable();
            $table->string('abdominal') -> nullable();
            $table->string('coxa') -> nullable();
            $table->timestamps();

            $table->foreign('aluno_id')->references('id')->on('users');
            $table->foreign('profissional_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('avaliacoes_fisicas');
    }
}
