<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDietasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dietas', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('id_profissional')->unsigned();
            $table->integer('id_aluno')->unsigned();
            $table->date('data');
            $table->string('descricao');
            $table->timestamps();

            $table -> foreign('id_profissional') -> references('id') -> on('users');
            $table -> foreign('id_aluno') -> references('id') -> on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dietas');
    }
}
