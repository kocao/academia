<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFinanceiroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financeiro', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('aluno_id')->unsigned();
            $table->date('dt_vcto')->nullable();
            $table->date('dt_pgto')->nullable();
            $table->double('vlr_vencer')->nullable();
            $table->double('vlr_pago')->nullable();
            $table->integer('sessoes')->nullable();
            $table->double('desconto')->nullable();
            $table->double('juros')->nullable();
            $table->integer('mes')->nullable();
            $table->integer('ano')->nullable();
            $table->timestamps();
            
            $table -> foreign('aluno_id') -> references('id') -> on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('financeiro');
    }
}
