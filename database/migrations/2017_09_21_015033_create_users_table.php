<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table) {
            $table->increments('id');
            $table->string('perfil');
            $table->string('name');
            $table->string('email');
            $table->integer('id_area_profissional')->unsigned()->nullable();
            $table->string('registro_profissional')->nullable();
            $table->string('telefone')->nullable();
            $table->string('whatsap')->nullable();
            $table->string('cpf');
            $table->string('dt_nasc');
            $table->string('password');
            $table->boolean('ativo')->default(true);
            $table->rememberToken();
            $table->timestamps();

            $table -> foreign('id_area_profissional') -> references('id') -> on('area_profissional');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
