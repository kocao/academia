<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTreinosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treinos', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('treinamento_id')->unsigned();
            $table->string('titulo_treino');
            $table->text('anotacoes')->nullable();
            $table->timestamps();

            $table -> foreign('treinamento_id') -> references('id') -> on('treinamentos') -> onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('treinos');
    }
}
