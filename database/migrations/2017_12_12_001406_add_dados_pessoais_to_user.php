<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDadosPessoaisToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('users', function (Blueprint $table) {
         $table -> string('sigla') -> nullable();
         $table -> string('contato_emer') -> nullable();
         $table -> string('medico') -> nullable();
         $table -> string('tipo_sang') -> nullable();
         $table -> text('alergias') -> nullable();
         $table -> string('plano_saude') -> nullable();
         $table -> string('hospital') -> nullable();
         $table -> boolean('concordo') -> nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('users', function (Blueprint $table) {
         $table -> dropColumn('sigla');
         $table -> dropColumn('contato_emer');
         $table -> dropColumn('medico');
         $table -> dropColumn('tipo_sang');
         $table -> dropColumn('alergias');
         $table -> dropColumn('plano_saude');
         $table -> dropColumn('hospital');
         $table -> dropColumn('concordo');
      });
    }
}
