<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDietaRefeicaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dieta_refeicoes', function (Blueprint $table) {
            $table -> increments('id');
            $table -> timestamps();
            $table -> integer('ordem_refeicao');
            $table -> integer('dieta_id')->unsigned();
            $table -> text('segunda') -> nullable();
            $table -> text('terca') -> nullable();
            $table -> text('quarta') -> nullable();
            $table -> text('quinta') -> nullable();
            $table -> text('sexta') -> nullable();
            $table -> text('sabado') -> nullable();
            $table -> text('domingo') -> nullable();
            $table -> text('opcao1') -> nullable();
            $table -> text('opcao2') -> nullable();
            $table -> text('opcao3') -> nullable();

            $table -> foreign('dieta_id')->references('id')->on('dietas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dieta_refeicoes');
    }
}
