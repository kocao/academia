<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAtividadePilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atividades_pil', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('treino_id') -> unsigned();
            $table->string('publico') -> nullable();
            $table->string('aquecimento') -> nullable();
            $table->string('tempo') -> nullable();
            $table->string('aparelho') -> nullable();
            $table->string('exercicio') -> nullable();
            $table->string('volta') -> nullable();
            $table->timestamps();

            $table -> foreign('treino_id') -> references('id') -> on('treinos') -> onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('atividades_pil');
    }
}
