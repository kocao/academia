<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTreinamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treinamentos', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('profissional_id')->unsigned();
            $table->integer('aluno_id')->unsigned();
            $table->integer('tipo_treino');
            $table->string('sublimiar1') -> nullable();
            $table->string('sublimiar2') -> nullable();
            $table->string('limiaraerobio') -> nullable();
            $table->string('supralimiar') -> nullable();
            $table->string('peso') -> nullable();
            $table->string('estatura') -> nullable();
            $table->string('objetivo')-> nullable();
            $table->timestamps();

            $table -> foreign('profissional_id') -> references('id') -> on('users');
            $table -> foreign('aluno_id') -> references('id') -> on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('treinamentos');
    }
}
