<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAtividadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('atividades', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('treino_id')->unsigned();
            $table->string('descricao') -> nullable();
            $table->string('distancia') -> nullable();
            $table->string('tempo') -> nullable();
            $table->string('pace') -> nullable();
            $table->timestamps();
            $table -> foreign('treino_id') -> references('id') -> on('treinos') -> onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('atividades');
    }
}
