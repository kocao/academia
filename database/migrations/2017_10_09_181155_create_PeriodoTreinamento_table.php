<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePeriodoTreinamentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periodos_treinamento', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('treinamento_id')->unsigned();
            $table->string('semana')->nullable();
            $table->string('segunda')->nullable();
            $table->string('terca')->nullable();
            $table->string('quarta')->nullable();
            $table->string('quinta')->nullable();
            $table->string('sexta')->nullable();
            $table->string('sabado')->nullable();
            $table->string('domingo')->nullable();
            $table->timestamps();

            $table -> foreign('treinamento_id') -> references('id') -> on('treinamentos') -> onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('periodos_treinamento');
    }
}
