<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exames', function(Blueprint $table) {
            $table->increments('id');
            $table->string('medico');
            $table->string('crm');
            $table->integer('id_user')->unsigned();
            $table->date('data');
            $table->text('diagnostico');
            $table->timestamps();

            $table -> foreign('id_user') -> references('id') -> on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('exames');
    }
}
