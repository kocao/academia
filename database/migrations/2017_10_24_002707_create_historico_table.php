<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historico', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('profissional_id') -> nullable();
            $table->integer('aluno_id') -> nullable();
            $table->date('data') -> nullable();
            $table->string('descricao') -> nullable();
            $table->string('link_aluno') -> nullable();
            $table->string('link_profissional') -> nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('historico');
    }
}
