<?php

use Illuminate\Database\Seeder;
use App\User;
use App\HorarioDisponivel;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::create(
              [
                  'name' => "Administrador do Sistema",
                  'email' => "admin@wellnesshealth.com.br",
                  'perfil' => "Administrador",
                  'cpf' => '222.333.444-65',
                  'dt_nasc' => '2000-01-01',
                  'ativo' => 1,
                  'password' => bcrypt('wh123@')
              ]
      );

      HorarioDisponivel::create(['hora' => '00:00', 'disponivel' => false]);
      HorarioDisponivel::create(['hora' => '00:30', 'disponivel' => false]);
      HorarioDisponivel::create(['hora' => '01:00', 'disponivel' => false]);
      HorarioDisponivel::create(['hora' => '01:30', 'disponivel' => false]);
      HorarioDisponivel::create(['hora' => '02:00', 'disponivel' => false]);
      HorarioDisponivel::create(['hora' => '02:30', 'disponivel' => false]);
      HorarioDisponivel::create(['hora' => '03:00', 'disponivel' => false]);
      HorarioDisponivel::create(['hora' => '03:30', 'disponivel' => false]);
      HorarioDisponivel::create(['hora' => '04:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '04:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '05:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '05:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '06:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '06:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '07:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '07:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '08:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '08:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '09:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '09:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '10:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '10:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '11:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '11:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '12:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '12:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '13:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '13:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '14:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '14:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '15:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '15:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '16:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '16:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '17:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '17:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '18:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '18:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '19:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '19:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '20:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '20:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '21:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '21:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '22:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '22:30', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '23:00', 'disponivel' => true]);
      HorarioDisponivel::create(['hora' => '23:30', 'disponivel' => true]);

      // User::create(
      //         [
      //             'name' => "Profissional",
      //             'email' => "prof@admin.com.br",
      //             'nivel' => 2,
      //             'password' => bcrypt('abap776')
      //         ]
      // );
      //
      // User::create(
      //         [
      //             'name' => "Usuário",
      //             'email' => "user@admin.com.br",
      //             'nivel' => 10,
      //             'password' => bcrypt('abap776')
      //         ]
      // );

    }
}
