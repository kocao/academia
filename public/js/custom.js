// function deleteImage(id){
//    $.post('file/delete', {id: id }, function(data){
//       alert(data);
//    });
// }
function data_picker(campo){
   $(campo).datepicker({
      dateFormat: 'dd/mm/yy',
      buttonText: "Selecione a data"
   });
}

function telefone_mask(campo){
   var v = campo.val();
   v=v.replace(/\D/g,"");                 //Remove tudo o que não é dígito
   v=v.replace(/^(\d\d)(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
   if (v.length > 13 ) {
      v=v.replace(/(\d{5})(\d)/,"$1-$2");
   } else {   //Coloca hífen entre o quarto e o quinto dígitos
      v=v.replace(/(\d{4})(\d)/,"$1-$2");   //Coloca hífen entre o quarto e o quinto dígitos
   }
   campo.val(v);
}

function cpf_mask(campo){
   var v = campo.val();
   v=v.replace(/\D/g,"") //Remove tudo o que não é dígito
   v=v.replace(/(\d{3})(\d)/,"$1.$2") //Coloca ponto entre o terceiro e o quarto dígitos
   v=v.replace(/(\d{3})(\d)/,"$1.$2") //Coloca ponto entre o setimo e o oitava dígitos
   v=v.replace(/(\d{3})(\d)/,"$1-$2") //Coloca ponto entre o decimoprimeiro e o decimosegundo dígitos
   campo.val(v);
return v
}

function numeros_mask(campo){
   var v = campo.val();
   v=v.replace(/([^0-9\.])/,"") //Remove tudo o que não é dígito

   campo.val(v);
}

function moeda(campo){
   var v = campo.val();
   if(v.length > 2){
     v=v.replace(/([^0-9])/,"") //Remove tudo o que não é dígito
     v=v.replace(/(\d*)(\d{2})/,"$1.$2") //Coloca ponto entre o terceiro e o quarto dígitos
     campo.val(v);
   }
}

function select_modal(element){
  var treino = element.text();
  var sigla = element.val();
  var btn = $('.btn-treino.selected');
  var hidden = $('.dia-treino.selected');
  var btnClass = 'btn-success';

  switch (treino) {
     case 'Vazio':
        btnClass = 'btn-default';
        break;
     case 'Descanso':
        btnClass = 'btn-info';
        break;
  }

  btn.text(sigla);
  hidden.val(sigla);

  btn.removeClass('selected btn-info btn-success btn-default');
  btn.addClass(btnClass);

  hidden.removeClass('selected');


}
