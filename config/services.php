<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
      'client_id' => '1960599637491378',
      'client_secret' => 'f30797990e2e970e643c61f52254ff82',
      'redirect' => 'http://appwh.wbctech.com.br/facebook',
    ],
    'google' => [
      'client_id' => '730902366876-c0clud34hs4jrf2p9par431r9eep5mpn.apps.googleusercontent.com',
      'client_secret' => 'qIzKdsP8-mIKJm0uP_GOZdxf',
      'redirect' => 'http://appwh.wbctech.com.br/google',
    ],

];
