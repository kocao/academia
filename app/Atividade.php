<?php

namespace App;
use App\Treino;

use Illuminate\Database\Eloquent\Model;

class Atividade extends Model
{
    protected $fillable = ['treino_id','descricao','distancia','pace','tempo'];

    public function treino(){
      return $this -> belongsTo('App\Treino');
    }
}
