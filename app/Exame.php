<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\File;

class Exame extends Model
{

  public static function boot()
  {
     parent::boot();

     static::created(function($exame)
     {
         $historicoData['descricao'] = "Foi criado um novo Exame por você.";
         $historicoData['data'] = date('Y-m-d');
         $historicoData['aluno_id'] = $exame -> id_user;

         Historico::create($historicoData);

     });

     static::updated(function($exame)
     {
       $historicoData['descricao'] = "Você atualizou um exame.";
       $historicoData['data'] = date('Y-m-d');
       $historicoData['aluno_id'] = $exame -> id_user;

       Historico::create($historicoData);

     });
  }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'exames';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['medico', 'crm', 'id_user', 'data', 'diagnostico',];

    public function aluno(){
      return $this -> belongsTo('App\User', 'id_user');
   }
   public function file(){
      return $this -> hasOne('App\File', 'id_exame');
   }
}
