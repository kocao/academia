<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profissional extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'profissionais';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id_usuario', 'id_area_profissional', 'registro_profissional',  'telefone', 'whatsap'];

    public function usuario(){
      return $this -> belongsTo('App\User', 'id_usuario');
    }

    public function area_profissional(){
      return $this -> belongsTo('App\AreaProfissional', 'id_area_profissional');
    }

}
