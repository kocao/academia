<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class OnlyAluno
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $perfil = Auth::user() -> perfil;
        if($perfil != 'Aluno'){
          return redirect('/historico');
        }


        return $next($request);
    }
}
