<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Session;


use App\Agendamento;
use App\HorarioDisponivel;
use App\User;
use App\Historico;

class AgendamentoController extends Controller
{

  public function __construct(){
    $this->middleware('only_profadmin', ['except' => 'indexAluno']);
  }

   public function create() {
      if(empty($_GET['data'])){
        $data = date('Y-m-d');
      } else {
        $data = db_date($_GET['data']);
      }

      $horarios = Agendamento::horarios_disponiveis($data);
      $alunos = User::alunosSelect();

      $dataAgendamento['data'] = $data;
      $dataAgendamento['horarios'] = $horarios;
      $dataAgendamento['alunos'] = $alunos;

      return view('agendamento.create', $dataAgendamento);
   }

   public function getHorariosView(Request $request) {
      $requestData = $request -> all();

      $data = db_date($requestData['data']);
      $horarios = Agendamento::horarios_disponiveis($data);
      $alunos = User::alunosSelect();

      $dataAgendamento['data'] = $data;
      $dataAgendamento['horarios'] = $horarios;
      $dataAgendamento['alunos'] = $alunos;

      return view('agendamento._horarios_view', $dataAgendamento);

   }

   public function indexAluno(){
      $agendamentos = Agendamento::where('aluno_id', Auth::id()) -> where ('data','>=', date('Y-m-d')) -> get();
      return view('agendamento.indexAluno',compact('agendamentos'));
   }

   public function store(Request $request)
   {
      $requestData = $request->all();
      // $requestData['aluno_id'] = Auth::id();
      $aluno_ids = $requestData['aluno_id'];
      $aluno_ids_old = [];
      $data = db_date($requestData['data']);
      $human_data = $requestData['data'];
      $horario_ids = $requestData['horario_id'];
      $descricao = $requestData['descricao'];

      /* Deletando agendamentos anteriores */
      // $agendamentos_antigos = Agendamento::where('data','=',db_date($data)) -> get();
      // foreach ($agendamentos_antigos as $key => $ag_antigo) {
      //    Agendamento::destroy($ag_antigo -> id);
      //
      //    if(!empty($ag_antigo -> aluno_id)){
      //      $aluno_ids_old[] = $ag_antigo -> id;
      //    }
      // }

      foreach ($aluno_ids as $key => $aluno_id) {
         $desc = $descricao[$key];
         $dataAgendamento['horario_disponivel_id'] = $horario_ids[$key];
         $dataAgendamento['aluno_id'] = $aluno_id;
         $dataAgendamento['data'] = $data;
         $dataAgendamento['descricao'] = $desc;

         $horario_antigo = Agendamento::where(['data' => $data, 'horario_disponivel_id' => $horario_ids[$key]]) -> first();
         if(!empty($horario_antigo)){
           if($horario_antigo -> aluno_id == $aluno_id && $horario_antigo -> descricao == $desc){
             continue;
           } else {
            Agendamento::destroy($horario_antigo -> id);
           }
         }
         if(!empty($aluno_id)){
            Agendamento::create($dataAgendamento);
            if(!in_array($aluno_id, $aluno_ids_old)){
              $hora = HorarioDisponivel::findOrFail($horario_ids[$key]) -> hora;
              $historico['descricao'] = "Consulta agendada para $human_data as $hora.";
              Historico::create($historico);
            }
         }
      }

      Session::flash('flash_message', 'Agendamento efetuado com sucesso!');
      return redirect('agendamento/create?data='.$requestData['data']);
   }

   public function destroy($id)
   {
      // Agendamento::destroy($id);
      //
      // Session::flash('flash_message', 'Agendamento excluído com sucesso !');
      //
      // return redirect('agendamento/indexAluno');
   }


}
