<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Exame;
use App\User;
use App\File;
use Illuminate\Http\Request;
use Session;

class ExameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
     public function __construct(){
      //  $this->middleware('only_aluno', ['except' => ['index','show']]);
     }


    public function index(Request $request)
    {
        if(!empty($_GET['id_user'])){
          $id_user = $_GET['id_user'];
        }

        $keyword = $request->get('search');
        $perPage = 25;
        $perfil = Auth::user() -> perfil;
        if($perfil == 'Aluno'){
          $id_user = Auth::id();
        }
        if (!empty($keyword)) {
            $exames = Exame::where('medico', 'LIKE', "%$keyword%")
				->orWhere('crm', 'LIKE', "%$keyword%")
				->orWhere('id_aluno', 'LIKE', "%$keyword%")
				->orWhere('data', 'LIKE', "%$keyword%")
				->orWhere('diagnostico', 'LIKE', "%$keyword%")
            ->where('id_user', $id_user)
				->paginate($perPage);
        } else {
               $exames = Exame::where('id_user', $id_user) -> paginate($perPage);
        }

        return view('exame.index', compact('exames'), compact('perfil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $alunos = User::alunosSelect();
        return view('exame.create', compact('alunos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();
        $image = $request->file('file');
        if(!empty($image)){
           $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
           $destinationPath = public_path('/images');
           $image -> move($destinationPath, $input['imagename']);
        }

        $requestData['data'] = db_date($requestData['data']);
        $requestData['id_user'] = Auth::id();
        $exame = Exame::create($requestData);

        if(!empty($image)){
           File::create(['id_exame' => $exame -> id, 'id_user' => $exame -> id_user, 'path_arquivo' => $destinationPath.'/'.$input['imagename'], 'url_arquivo' => url('images/'.$input['imagename'])]);
        }

        Session::flash('flash_message', 'Exame added!');

        return redirect('exame');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $exame = Exame::findOrFail($id);

        return view('exame.show', compact('exame'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $exame = Exame::findOrFail($id);
        $alunos = User::alunosSelect();
        $arquivo = $exame -> file;
        $data['exame'] = $exame;
        $data['alunos'] = $alunos;
        $data['arquivo'] = $arquivo;

        return view('exame.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $requestData = $request->all();
        $image = $request->file('file');
        if(!empty($image)){
           $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
           $destinationPath = public_path('/images');
           $image -> move($destinationPath, $input['imagename']);
        }

        $exame = Exame::findOrFail($id);
        $exame->update($requestData);

        if(!empty($image)){
           File::create(['id_exame' => $exame -> id, 'id_user' => $exame -> id_user, 'path_arquivo' => $destinationPath.'/'.$input['imagename'], 'url_arquivo' => url('images/'.$input['imagename'])]);
        }
        Session::flash('flash_message', 'Exame updated!');

        return redirect('exame');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Exame::destroy($id);

        Session::flash('flash_message', 'Exame deleted!');

        return redirect('exame');
    }

    public function delete_file(Request $request){
      $requestData = $request->all();
      $file = File::findOrFail($requestData['id']);
      $file -> deleteImage();
      echo "deletado com sucesso";
    }

    public function index_profissional(){
       $alunos = DB::table('users') -> join('exames', 'users.id', '=', 'exames.id_user')->select('users.id', 'users.name')->get();

       return view('exame.index_profissional', compact('alunos'));
    }

}
