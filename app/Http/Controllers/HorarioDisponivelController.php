<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HorarioDisponivel;
class HorarioDisponivelController extends Controller
{

    public function __construct(){
      $this->middleware('only_admin');
    }

    public function index(){
      $horarios = HorarioDisponivel::all();

      return view('horario-disponivel.index', compact('horarios'));

    }

    public function update(Request $request){
      $requestData = $request -> all();
      $disponivel = $requestData['disponivel'];
      $id = $requestData['id'];
      $horario_disponivel = HorarioDisponivel::findOrFail($id);
      $horario_disponivel -> update(['disponivel' => $disponivel]);
    }
}
