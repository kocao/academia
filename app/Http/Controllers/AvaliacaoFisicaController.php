<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\AvaliacaoFisica;
use App\User;
use Illuminate\Http\Request;
use Session;

class AvaliacaoFisicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
     public function __construct(){
       $this->middleware('only_profadmin', ['except' => 'indexAluno']);
     }

    public function indexAluno(Request $request){
      $avaliacaofisica = AvaliacaoFisica::where('aluno_id' ,'=', Auth::id()) -> paginate(3);
      $diferenca = [null];

      $diferenca1 = new AvaliacaoFisica();
      $diferenca2 = new AvaliacaoFisica();
      if(!empty($avaliacaofisica -> first())){
        $at_1 = $avaliacaofisica -> first() -> getAttributes();
      }
      if(!empty($avaliacaofisica[1])){
        foreach ($at_1 as $key => $value) {
          if(in_array($key,["id", "id_aluno", "data", "created_at","updated_at"]))
            continue;

          eval("\$value_1 = \$avaliacaofisica[0] -> $key;");
          eval("\$value_2 = \$avaliacaofisica[1] -> $key;");
          eval("\$diferenca1 -> $key = intval(\$value_1) - intval(\$value_2);");
        }
        $diferenca[] = $diferenca1;
      }

      if(!empty($avaliacaofisica[2])){
        foreach ($at_1 as $key => $value) {
          if(in_array($key,["id", "id_aluno", "data", "created_at","updated_at"]))
            continue;

          eval("\$value_1 = \$avaliacaofisica[1] -> $key;");
          eval("\$value_2 = \$avaliacaofisica[2] -> $key;");
          eval("\$diferenca2 -> $key = intval(\$value_1) - intval(\$value_2);");
        }
        $diferenca[] = $diferenca2;
      }

      return view('avaliacao-fisica.indexAluno', compact('avaliacaofisica'), compact('diferenca'));
    }

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $avaliacaofisica = AvaliacaoFisica::where('peso', 'LIKE', "%$keyword%")
				->orWhere('id_aluno', 'LIKE', "%$keyword%")
				->orWhere('id_profissional', 'LIKE', "%$keyword%")
				->orWhere('estatura', 'LIKE', "%$keyword%")
				->orWhere('gor_ideal', 'LIKE', "%$keyword%")
				->orWhere('gor_atual', 'LIKE', "%$keyword%")
				->orWhere('peso_magro', 'LIKE', "%$keyword%")
				->orWhere('peso_gordo', 'LIKE', "%$keyword%")
				->orWhere('peso_residual', 'LIKE', "%$keyword%")
				->orWhere('peso_muscular', 'LIKE', "%$keyword%")
				->orWhere('per_muscular', 'LIKE', "%$keyword%")
				->orWhere('torax', 'LIKE', "%$keyword%")
				->orWhere('cintura', 'LIKE', "%$keyword%")
				->orWhere('abdomen', 'LIKE', "%$keyword%")
				->orWhere('quadril', 'LIKE', "%$keyword%")
				->orWhere('coxa_direita', 'LIKE', "%$keyword%")
				->orWhere('coxa_esquerda', 'LIKE', "%$keyword%")
				->orWhere('pantu_direita', 'LIKE', "%$keyword%")
				->orWhere('pantu_esquerda', 'LIKE', "%$keyword%")
				->orWhere('ante_direito', 'LIKE', "%$keyword%")
				->orWhere('ante_esquerdo', 'LIKE', "%$keyword%")
				->orWhere('braco_direito', 'LIKE', "%$keyword%")
				->orWhere('braco_esquerdo', 'LIKE', "%$keyword%")
				->orWhere('subscapular', 'LIKE', "%$keyword%")
				->orWhere('tricipital', 'LIKE', "%$keyword%")
				->orWhere('peitoral', 'LIKE', "%$keyword%")
				->orWhere('axilar', 'LIKE', "%$keyword%")
				->orWhere('supra_iliaca', 'LIKE', "%$keyword%")
				->orWhere('abdominal', 'LIKE', "%$keyword%")
				->orWhere('coxa', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $avaliacaofisica = AvaliacaoFisica::paginate($perPage);
        }

        return view('avaliacao-fisica.index', compact('avaliacaofisica'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $alunos = User::alunosSelect();
        return view('avaliacao-fisica.create',compact('alunos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();
        $requestData['data'] = db_date($requestData['data']);
        $requestData['profissional_id'] = Auth::id();
        AvaliacaoFisica::create($requestData);

        Session::flash('flash_message', 'AvaliacaoFisica added!');

        return redirect('avaliacao-fisica');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $alunos = User::alunosSelect();
        $avaliacaofisica = AvaliacaoFisica::findOrFail($id);

        return view('avaliacao-fisica.show', compact('avaliacaofisica'), compact('alunos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $avaliacaofisica = AvaliacaoFisica::findOrFail($id);

        $avaliacaofisica -> data = human_date($avaliacaofisica -> data);

        return view('avaliacao-fisica.edit', compact('avaliacaofisica'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();
        $requestData['data'] = db_date($requestData['data']);


        $avaliacaofisica = AvaliacaoFisica::findOrFail($id);
        $avaliacaofisica->update($requestData);

        Session::flash('flash_message', 'AvaliacaoFisica updated!');

        return redirect('avaliacao-fisica');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        AvaliacaoFisica::destroy($id);

        Session::flash('flash_message', 'AvaliacaoFisica deleted!');

        return redirect('avaliacao-fisica');
    }
}
