<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Treinamento;
use App\User;
use App\Treino;
use App\Atividade;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use Session;

class TreinamentoController extends Controller
{


  public function __construct(){
    $this->middleware('only_profadmin', ['except' => ['indexAluno','show']]);
  }

   /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\View\View
   */
   public function index(Request $request)
   {
      $keyword = $request->get('search');
      $perPage = 25;

      if (!empty($keyword)) {
         $treinamento = Treinamento::where('profissional_id', 'LIKE', "%$keyword%")
         ->orWhere('aluno_id', 'LIKE', "%$keyword%")
         ->orWhere('tipo_treino', 'LIKE', "%$keyword%")
         ->orWhere('sublimiar1', 'LIKE', "%$keyword%")
         ->orWhere('sublimiar2', 'LIKE', "%$keyword%")
         ->orWhere('limiaraerobio', 'LIKE', "%$keyword%")
         ->orWhere('supralimiar', 'LIKE', "%$keyword%")
         ->orWhere('peso', 'LIKE', "%$keyword%")
         ->orWhere('estatura', 'LIKE', "%$keyword%")
         ->orWhere('objetivo', 'LIKE', "%$keyword%")
         ->paginate($perPage);
      } else {
         $treinamento = Treinamento::paginate($perPage);
      }

      return view('treinamento.index', compact('treinamento'));
   }

   public function indexAluno($tipo_treino)
   {
      $treinamento = Treinamento::where('aluno_id', '=', Auth::id())
      ->where('tipo_treino','=',$tipo_treino)->get();

      return view('treinamento.index', compact('treinamento'));
   }

   /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\View\View
   */
   public function create()
   {
      $alunos = User::alunosSelect();
      $tipos_treino = Treinamento::tipos_treino();
      $treinamento = new Treinamento([],1);

      $data['alunos'] = $alunos;
      $data['tipos_treino'] = $tipos_treino;
      $data['treinamento'] = $treinamento;
      $data['view_treino'] = "treinamento._treinos_padrao";

      return view('treinamento.create', $data);
   }

   /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */
   public function store(Request $request)
   {

      $requestData = $request->all();
      $requestData['profissional_id'] = Auth::id();
      Treinamento::create_or_update($requestData);

      Session::flash('flash_message', 'Treinamento added!');

      return redirect('treinamento');
   }

   /**
   * Display the specified resource.
   *
   * @param  int  $id
   *
   * @return \Illuminate\View\View
   */
   public function show($id)
   {
      $treinamento = Treinamento::findOrFail($id);
      $tipos_treino = Treinamento::tipos_treino();

      $view_treino = "treinamento._treinos_show_padrao";
      $tipo_treino = $treinamento -> tipo_treino;

      if($tipo_treino == 3)
         $view_treino = "treinamento._treinos_show_mus";
      if($tipo_treino == 5)
         $view_treino = "treinamento._treinos_show_pil";

      $data['treinamento'] = $treinamento;
      $data['tipos_treino'] = $tipos_treino;
      $data['view_treino'] = $view_treino;

      return view('treinamento.show', $data);
   }

   /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   *
   * @return \Illuminate\View\View
   */
   public function edit($id)
   {
      $alunos = User::alunosSelect();
      $treinamento = Treinamento::findOrFail($id);
      $tipos_treino = Treinamento::tipos_treino();

      $view_treino = "treinamento._treinos_padrao";
      $tipo_treino = $treinamento -> tipo_treino;

      if($tipo_treino == 3)
         $view_treino = "treinamento._treinos_musculacao";
      if($tipo_treino == 5)
         $view_treino = "treinamento._treinos_pilates";

      $data['treinamento'] = $treinamento;
      $data['tipos_treino'] = $tipos_treino;
      $data['alunos'] = $alunos;
      $data['view_treino'] = $view_treino;
      $data['periodos'] = $treinamento -> periodos_treinamento;

      return view('treinamento.edit', $data);
   }

   /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @param \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */
   public function update($id, Request $request)
   {

      $requestData = $request->all();

      Treinamento::create_or_update($requestData);

      Session::flash('flash_message', 'Treinamento updated!');

      return redirect('treinamento');
   }

   /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   *
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */
   public function destroy($id)
   {
      Treinamento::destroy($id);

      Session::flash('flash_message', 'Treinamento deleted!');

      return redirect('treinamento');
   }

   public function view_treino(){
      $view_treino = "treinamento._treinos_padrao";
      $tipo_treino = $_POST['tipo_treino'];
      $treinamento = new Treinamento([],$tipo_treino);
      if($tipo_treino == 3)
         $view_treino = "treinamento._treinos_musculacao";
      if($tipo_treino == 5)
         $view_treino = "treinamento._treinos_pilates";

      return view($view_treino, compact('treinamento'));
   }

   public function modal_periodo(){
      $view_treino = "treinamento._modal_periodo";
      $tipo_treino = $_POST['tipo_treino'];
      $treinamento = new Treinamento([],$tipo_treino);

      return view($view_treino, compact('treinamento'));
   }
}
