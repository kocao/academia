<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\AreaProfissional;
use Illuminate\Http\Request;
use Session;

class AreaProfissionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
     public function __construct(){
       $this->middleware('only_admin');
     }

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $areaprofissional = AreaProfissional::where('descr_area', 'LIKE', "%$keyword%")
				->orWhere('sigla_registro', 'LIKE', "%$keyword%")
				->orWhere('registro', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $areaprofissional = AreaProfissional::paginate($perPage);
        }

        return view('area-profissional.index', compact('areaprofissional'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('area-profissional.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        AreaProfissional::create($requestData);

        Session::flash('flash_message', 'AreaProfissional added!');

        return redirect('admin/area-profissional');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $areaprofissional = AreaProfissional::findOrFail($id);

        return view('area-profissional.show', compact('areaprofissional'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $areaprofissional = AreaProfissional::findOrFail($id);

        return view('area-profissional.edit', compact('areaprofissional'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $areaprofissional = AreaProfissional::findOrFail($id);
        $areaprofissional->update($requestData);

        Session::flash('flash_message', 'AreaProfissional updated!');

        return redirect('admin/area-profissional');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        AreaProfissional::destroy($id);

        Session::flash('flash_message', 'AreaProfissional deleted!');

        return redirect('admin/area-profissional');
    }
}
