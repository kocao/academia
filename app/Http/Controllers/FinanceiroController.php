<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Session;

use App\Financeiro;
use App\User;


class FinanceiroController extends Controller
{
  public function __construct(){
    $this->middleware('only_profadmin');
  }


   function index(){
      if(empty($_GET['mes'])){
         $mes = date('m');
         $ano = date('Y');
      } else {
         $mes = $_GET['mes'];
         $ano = $_GET['ano'];
      }

      $contas = Financeiro::where(['mes' => $mes, 'ano' => $ano]) -> get();
      $alunos = User::alunosSelect();
      $profissionais = User::profissionaisSelect();

      $data['contas'] = $contas;
      $data['profissionais'] = $profissionais;
      $data['alunos'] = $alunos;
      $data['mes'] = $mes;
      $data['ano'] = $ano;

      return view('financeiro.index', $data);
   }

   public function store(Request $request)
   {
      $requestData = $request->all();

      $mes = $requestData['mes'];
      $ano = $requestData['ano'];

      /* Deletando registros financeiros anteriores */
      // $contas = Financeiro::where(['mes' => $mes, 'ano' => $ano]) -> get();
      // foreach ($contas as $key => $conta) {
      //    Financeiro::destroy($conta -> id);
      // }

      if(!empty($requestData['aluno_id'])){
         $aluno_ids = $requestData['aluno_id'];

         foreach ($aluno_ids as $key => $aluno_id) {
            $dataFinanceiro['id'] = $requestData['financeiro_id'][$key];
            $dataFinanceiro['aluno_id'] = $aluno_id;
            $dataFinanceiro['mes'] = $mes;
            $dataFinanceiro['ano'] = $ano;
            $dataFinanceiro['dt_vcto'] = db_date($requestData['dt_vcto'][$key]);
            $dataFinanceiro['vlr_pago'] = $requestData['vlr_pago'][$key];
            $dataFinanceiro['dt_pgto'] = db_date($requestData['dt_pgto'][$key]);
            $dataFinanceiro['vlr_vencer'] = $requestData['vlr_vencer'][$key];
            $dataFinanceiro['recebeu'] = $requestData['recebeu'][$key];
            $dataFinanceiro['profissional_id'] = $requestData['profissional_id'][$key];

            if(empty($dataFinanceiro['id'])){
              Financeiro::create($dataFinanceiro);
            } else {
              $financeiro = Financeiro::findOrFail($dataFinanceiro['id']);
              $financeiro -> update($dataFinanceiro);
            }
         }
      }

      Session::flash('flash_message', 'Agendamento efetuado com sucesso!');
      return redirect("financeiro?ano=$ano&mes=$mes");
   }

   function add_conta(){
      $conta = new Financeiro();
      $alunos = User::alunosSelect();

      return view('financeiro._conta_financeiro', compact('conta'), compact('alunos'));
   }

   function add_receber(){
      $conta = new Financeiro();
      $profissionais = User::profissionaisSelect();

      return view('financeiro._linha_receber', compact('conta'), compact('profissionais'));
   }

   public function destroy($id)
   {
       Financeiro::destroy($id);
   }

}
