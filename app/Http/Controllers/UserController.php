<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


use App\User;
use App\AreaProfissional;
use Illuminate\Http\Request;
use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
     public function __construct(){
       $this->middleware('only_admin', ['except' => ['dados_pessoais','edit_aluno','update_aluno']]);
       $this->middleware('only_aluno', ['only' => ['edit_aluno']]);


     }


    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $user = User::where('id_perfil', 'LIKE', "%$keyword%")
				->orWhere('name', 'LIKE', "%$keyword%")
				->orWhere('email', 'LIKE', "%$keyword%")
				->orWhere('id_area_profissional', 'LIKE', "%$keyword%")
				->orWhere('registro_profissional', 'LIKE', "%$keyword%")
				->orWhere('telefone', 'LIKE', "%$keyword%")
				->orWhere('whatsap', 'LIKE', "%$keyword%")
				->orWhere('cpf', 'LIKE', "%$keyword%")
				->orWhere('dt_nasc', 'LIKE', "%$keyword%")
				->orWhere('password', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $user = User::paginate($perPage);
        }

        return view('user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
      $perfil = Auth::user() -> perfil;
      if($perfil != 'Administrador'){
        return redirect('/historico');
      }
      $areas_profissionais = AreaProfissional::areaSelect();
      return view('user.create',compact('areas_profissionais'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
      $perfil = Auth::user() -> perfil;
      if($perfil != 'Administrador'){
        return redirect('/historico');
      }

        $requestData = $request->all();
        $requestData['password'] = bcrypt($requestData['password']);
        $requestData['dt_nasc'] = db_date($requestData['dt_nasc']);

        User::create($requestData);
        Session::flash('flash_message', 'Usuário criado com sucesso!');

        return redirect('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
      $perfil = Auth::user() -> perfil;
      if($perfil != 'Administrador'){
        return redirect('/historico');
      }

      $user = User::findOrFail($id);

      return view('user.show', compact('user'));
    }

    public function dados_pessoais()
    {
        $user = User::findOrFail(Auth::id());

        return view('user.dados-pessoais', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $perfil = Auth::user() -> perfil;
        if($perfil != 'Administrador'){
          return redirect('/historico');
        }


        $user = User::findOrFail($id);
        $user -> dt_nasc = human_date($user -> dt_nasc);
        $areas_profissionais = AreaProfissional::areaSelect();
        return view('user.edit', compact('user'), compact('areas_profissionais'));
    }

    public function edit_aluno()
    {
        $id = Auth::id();
        $user = User::findOrFail($id);
        $user -> dt_nasc = human_date($user -> dt_nasc);
        return view('user.edit_aluno', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
      $perfil = Auth::user() -> perfil;
      if($perfil != 'Administrador'){
        return redirect('/historico');
      }


        $requestData = $request->all();

        $user = User::findOrFail($id);
        $user->update($requestData);

        Session::flash('flash_message', 'Usuário atualizado com sucesso!');

        return redirect('user');
    }

    public function update_aluno( Request $request)
    {
        $id = Auth::id();
        $requestData = $request->all();

        $user = User::findOrFail($id);
        if(empty($requestData['concordo'])){
           $requestData['concordo'] = '0';
        }
        $user->update($requestData);

        Session::flash('flash_message', 'Usuário atualizado com sucesso!');

        return redirect('dados-pessoais');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $deletou = true;
        $perfil = Auth::user() -> perfil;
        if($perfil != 'Administrador'){
          return redirect('/historico');
        }

        try {
          User::destroy($id);
        } catch ( \Illuminate\Database\QueryException $e) {
            $deletou = false;
            if($e->errorInfo[0] == '23000')
              Session::flash('flash_error', 'O usuário tem informações relacionadas a ele e não pode ser excluído.');
        }

        if($deletou)
          Session::flash('flash_message', 'Usuário Deletado com sucesso!');

        return redirect('user');
    }
}
