<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialize;
use Session;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;


class RedesSociaisController extends Controller
{
      public function loginFacebook()
      {
         return Socialize::driver('facebook')->redirect();
      }

      public function loginGoogle()
      {
         return Socialize::driver('google')->redirect();
      }

      public function pageFacebook()
      {
          $userFacebook = Socialize::with('facebook')->user();

          $user = User::where(['provider' => 'Facebook', 'provider_id' => $userFacebook -> id])->first();

          if(empty($user)){
            $user = new User();
            $user -> name = $userFacebook -> name;
            $user -> email = $userFacebook -> email;
            $user -> password = bcrypt('wellness-default');
            $user -> perfil = "Aluno";
            $user -> provider = "Facebook";
            $user -> provider_id = $userFacebook -> id;
            $user -> save();
          }

          Auth::login($user);

          return redirect('/');

      }

      public function pageGoogle(){
        $userGoogle = Socialize::with('google')->user();
        $user = User::where(['provider' => 'Google', 'provider_id' => $userGoogle -> id])->first();

        if(empty($user)){
          $user = new User();
          $user -> name = $userGoogle -> name;
          $user -> email = $userGoogle -> email;
          $user -> password = bcrypt('wellness-default');
          $user -> perfil = "Aluno";
          $user -> provider = "Google";
          $user -> provider_id = $userGoogle -> id;
          $user -> save();
        }

        Auth::login($user);
        return redirect('/');
      }
}
