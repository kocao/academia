<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Dieta;
use App\DietaRefeicao;
use App\User;
use Illuminate\Http\Request;
use Session;

class DietaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
     public function __construct(){
       $this->middleware('only_profadmin', ['except' => ['show','index']]);
     }

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $perfil = Auth::user() -> perfil;

        if (!empty($keyword)) {
            $dieta = Dieta::where('id_profissional', 'LIKE', "%$keyword%")
				->orWhere('id_aluno', 'LIKE', "%$keyword%")
				->orWhere('data', 'LIKE', "%$keyword%")
				->orWhere('descricao', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            if($perfil == "Aluno"){
               $dieta = Dieta::where('id_aluno','=',Auth::id())->paginate($perPage);
            } else {
               $dieta = Dieta::paginate($perPage);
            }
        }

        return view('dieta.index', compact('dieta'),compact('perfil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $alunos = User::alunosSelect();
        $refeicoes = Dieta::refeicoes();
        return view('dieta.create',compact('alunos'),compact('refeicoes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $requestData['id_profissional'] = Auth::id();
        $requestData['data'] = db_date($requestData['data']);
        $refeicoes = Dieta::refeicoes();

        $dieta = Dieta::create($requestData);
        foreach ($refeicoes as $key => $refeicao) {
           $data['ordem_refeicao'] = $key;
           $data['dieta_id'] = $dieta -> id;
           $data['segunda'] = $requestData['segunda'][$key];
           $data['terca'] = $requestData['terca'][$key];
           $data['quarta'] = $requestData['quarta'][$key];
           $data['quinta'] = $requestData['quinta'][$key];
           $data['sexta'] = $requestData['sexta'][$key];
           $data['sabado'] = $requestData['sabado'][$key];
           $data['domingo'] = $requestData['domingo'][$key];
           $data['opcao1'] = $requestData['opcao1'][$key];
           $data['opcao2'] = $requestData['opcao2'][$key];
           $data['opcao3'] = $requestData['opcao3'][$key];
           DietaRefeicao::create($data);
        }

        Session::flash('flash_message', 'Dieta added!');

        return redirect('dieta');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $dieta = Dieta::findOrFail($id);
        $refeicoes = Dieta::refeicoes();


        return view('dieta.show', compact('dieta'),compact('refeicoes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $dieta = Dieta::findOrFail($id);
        $dieta -> data = human_date($dieta -> data);
        $dieta_refeicoes = $dieta -> dieta_refeicoes;
        $refeicoes = Dieta::refeicoes();
        $data['dieta'] = $dieta;
        $data['dieta_refeicoes'] = $dieta_refeicoes;
        $data['refeicoes'] = $refeicoes;

        return view('dieta.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();
        $dieta = Dieta::findOrFail($id);
        $requestData['id_profissional'] = Auth::id();
        $requestData['data'] = db_date($requestData['data']);
        $refeicoes = Dieta::refeicoes();

        $dieta->update($requestData);
        foreach ($refeicoes as $key => $refeicao) {
           $data['ordem_refeicao'] = $key;
           $data['dieta_id'] = $dieta -> id;
           $data['segunda'] = $requestData['segunda'][$key];
           $data['terca'] = $requestData['terca'][$key];
           $data['quarta'] = $requestData['quarta'][$key];
           $data['quinta'] = $requestData['quinta'][$key];
           $data['sexta'] = $requestData['sexta'][$key];
           $data['sabado'] = $requestData['sabado'][$key];
           $data['domingo'] = $requestData['domingo'][$key];
           $data['opcao1'] = $requestData['opcao1'][$key];
           $data['opcao2'] = $requestData['opcao2'][$key];
           $data['opcao3'] = $requestData['opcao3'][$key];
           $dieta_refeicao = DietaRefeicao::findOrFail($requestData['id'][$key]);
           $dieta_refeicao -> update($data);
        }


        Session::flash('flash_message', 'Dieta updated!');

        return redirect('dieta');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Dieta::destroy($id);

        Session::flash('flash_message', 'Dieta deleted!');

        return redirect('dieta');
    }
}
