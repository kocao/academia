<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Historico;


class HistoricoController extends Controller
{

  public function index(){
    $perfil = Auth::user() -> perfil;

    if($perfil == "Aluno"){
      $historicos = Historico::where('aluno_id','=',Auth::id()) -> orderBy('updated_at','desc') -> limit(10) -> get();
    } else {
      $historicos = Historico::where('profissional_id','=',Auth::id()) -> orderBy('updated_at','desc') -> limit(10) -> get();
    }
    // var_dump($historicos);
    return view('historico.index', compact('historicos'), compact('perfil'));
  }
    //
}
