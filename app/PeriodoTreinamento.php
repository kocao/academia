<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeriodoTreinamento extends Model
{
   protected $table = 'periodos_treinamento';
   protected $fillable = ['treinamento_id','semana','segunda' , 'terca' , 'quarta' , 'quinta' , 'sexta' , 'sabado' , 'domingo'];

   public function treinamento(){
      return $this -> belontsTo('App\Treinamento');
   }

}
