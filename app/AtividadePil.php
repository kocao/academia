<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AtividadePil extends Model
{
    protected $table = "atividades_pil";
    protected $fillable = ["treino_id","publico","aquecimento","tempo","aparelho","exercicio","volta"];
}
