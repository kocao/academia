<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Treino;
use App\Atividade;
use App\AtividadeMus;
use App\User;
use App\PeriodoTreino;

class Treinamento extends Model
{
    const tipos_treino = [1 => 'Corrida', 2 => 'Ciclismo', 3 => 'Musculação', 4 => 'Natação',5 => 'Pilates', 6 => 'Triatlon'];
    protected $table = 'treinamentos';

    protected $fillable = ['profissional_id', 'aluno_id', 'tipo_treino', 'sublimiar1', 'sublimiar2', 'limiaraerobio', 'supralimiar', 'peso', 'estatura', 'objetivo'];

    public function __construct($attributes = array(), $tipo_treino = null)  {
         parent::__construct($attributes);
         if($tipo_treino != null){
            $this -> tipo_treino = $tipo_treino;
            switch ($tipo_treino) {
               case 3:
                  $treinos = ['TA' => 'Treino A', 'TB' => 'Treino B', 'TC' => 'Treino C','TD' => 'Treino D'];
                  break;
               case 6:
                  $treinos = ['TA' => 'Treino A - Natação', 'TB' => 'Treino B - Ciclismo', 'TC' => 'Treino C - Corrida'];
                  break;
               case 5:
                  $treinos = ['INI' => 'Iniciante','INT' => 'Intermediário','AVA' => 'Avançado'];
                  break;
               default:
                  $treinos = ['TA' => 'Treino A','TB' => 'Treino B', 'TC' => 'Treino C'];
                  break;
            }

            foreach($treinos as $sigla => $treino){
               $this -> treinos -> push(new Treino(['titulo_treino' => $treino, 'sigla' => $sigla], true));
            }
         }
    }

    public static function boot()
    {
       parent::boot();

       static::created(function($treinamento)
       {
           $historicoTipoTreino = self::tipos_treino[$treinamento -> tipo_treino];

           $historicoData['descricao'] = "Foi criado um novo Treinamento de $historicoTipoTreino para você.";
           $historicoData['profissional_id'] = $treinamento -> profissional_id;
           $historicoData['data'] = date('Y-m-d');
           $historicoData['aluno_id'] = $treinamento -> aluno_id;
           $historicoData['link_aluno'] = 'treinamento/' . $treinamento -> id;
           $historicoData['link_profissional'] = 'treinamento/' . $treinamento -> id;

           Historico::create($historicoData);
       });

       static::saving(function($treinamento)
       {
         $historicoTipoTreino = self::tipos_treino[$treinamento -> tipo_treino];

         $historicoData['descricao'] = "Você teve um  Treinamento de $historicoTipoTreino atualizado.";
         $historicoData['profissional_id'] = $treinamento -> profissional_id;
         $historicoData['data'] = date('Y-m-d');
         $historicoData['aluno_id'] = $treinamento -> aluno_id;
         $historicoData['link_aluno'] = 'treinamento/' . $treinamento -> id;
         $historicoData['link_profissional'] = 'treinamento/' . $treinamento -> id;

         Historico::create($historicoData);

       });
    }

    public static function tipos_treino(){
      return self::tipos_treino;
    }

    public function treinos(){
      return $this -> hasMany('App\Treino');
    }

    public function treinos_mus(){
      return $this -> hasMany('App\TreinoMus');
    }

    public function periodos_treinamento(){
      return $this -> hasMany('App\PeriodoTreinamento');
    }

    public function profissional(){
      return $this -> belongsTo('App\User','profissional_id');
    }

    public function aluno(){
      return $this -> belongsTo('App\User','aluno_id');
    }

    public static function create_or_update($requestData){

      if(empty($requestData['id'])){
         $treinamento = Treinamento::create($requestData);
         $historicoData['descricao'] = "criado";
      } else {
         $historicoData['descricao'] = "alterado";
         $treinamento = Treinamento::findOrFail($requestData['id']);
         $treinamento -> update($requestData);
      }

      // $historicoTipoTreino = self::tipos_treino[$treinamento -> tipo_treino];
      //
      // $historicoData['descricao'] = "Treinamento de $historicoTipoTreino ". $historicoData['descricao'].".";
      // $historicoData['profissional_id'] = $treinamento -> profissional_id;
      // $historicoData['data'] = date('Y-m-d');
      // $historicoData['aluno_id'] = $treinamento -> aluno_id;

      Historico::create($historicoData);

      /* Treinos */
      foreach ($requestData['treino'] as $key => $treino) {
         $dataTreino['id'] = $treino['id_treino'];
         $dataTreino['titulo_treino'] = $treino['titulo_treino'];
         $dataTreino['sigla'] = $treino['sigla'];
         if(!empty($treino['anotacoes'])) {
            $dataTreino['anotacoes'] = $treino['anotacoes'];
         }

         $dataTreino['treinamento_id'] = $treinamento -> id;


         if(empty($dataTreino['id'])){
            $novo_treino = Treino::create($dataTreino);
         } else {
            $novo_treino = Treino::findOrFail($dataTreino['id']);
            $novo_treino -> update($dataTreino);
         }

         /* Atividades */
         $classe_atividade = "App\Atividade";
         if ($treinamento -> tipo_treino == 3)
            $classe_atividade = "App\AtividadeMus";
         if ($treinamento -> tipo_treino == 5)
            $classe_atividade = "App\AtividadePil";

         $keys_atividades = [];
         foreach ($treino as $key_atividade => $treino_teste) {
            if(is_array($treino_teste)){
               $keys_atividades[] = $key_atividade;
            }
         }

         foreach ($treino[$keys_atividades[0]] as $key => $descricao) {
            $dataAtividade = [];
            $dataAtividade['treino_id'] = $novo_treino -> id;
            foreach($keys_atividades as $key_atividade){
               $dataAtividade[$key_atividade] = $treino[$key_atividade][$key];
            }
            $dataAtividade['id'] = $dataAtividade['id_atividade'];
            if(empty($dataAtividade['id'])){
               eval("$classe_atividade::create(\$dataAtividade);");
            } else {
               eval("\$atividade_update = $classe_atividade::findOrFail( \$dataAtividade['id']);");
               $atividade_update -> update($dataAtividade);
            }
         }
      }

      /* PeriodoTreino */
      if ($treinamento -> tipo_treino != 5) {
        foreach ($requestData['segunda'] as $key => $value) {
           $dataPeriodo['treinamento_id'] = $treinamento -> id;
           $dataPeriodo['id'] = $requestData['id_periodo'][$key];
           $dataPeriodo['semana'] = $requestData['semana'][$key];
           $dataPeriodo['segunda'] = $requestData['segunda'][$key];
           $dataPeriodo['terca'] = $requestData['terca'][$key];
           $dataPeriodo['quarta'] = $requestData['quarta'][$key];
           $dataPeriodo['quinta'] = $requestData['quinta'][$key];
           $dataPeriodo['sexta'] = $requestData['sexta'][$key];
           $dataPeriodo['sabado'] = $requestData['sabado'][$key];
           $dataPeriodo['domingo'] = $requestData['domingo'][$key];

           if(empty($dataPeriodo['id'])){
              PeriodoTreinamento::create($dataPeriodo);
           } else {
              $periodo_update = PeriodoTreinamento::findOrFail($dataPeriodo['id']);
              $periodo_update -> update($dataPeriodo);
           }
        }
      }
    }

}
