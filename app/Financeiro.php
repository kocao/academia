<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Financeiro extends Model
{
   protected $table = 'financeiro';

   protected $fillable = ['aluno_id', 'dt_pgto', 'dt_vcto', 'juros', 'desconto',
    'vlr_pago', 'vlr_vencer', 'sessoes','mes','ano','profissional_id','recebeu'];

  public function aluno(){
    return $this -> belongsTo('App\User', 'aluno_id');
  }

  public function profissional(){
    return $this -> belongsTo('App\User', 'profissional_id');
  }

}
