<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\HorarioDisponivel;

class Agendamento extends Model
{
  public static function boot()
  {
     parent::boot();

     static::created(function($agendamento)
     {
         $data = human_date($agendamento -> data);
         $hora = $agendamento -> horario -> hora;
         $historicoData['descricao'] = "Foi agendada uma consulta para você no dia $data as $hora horas.";
         $historicoData['data'] = date('Y-m-d');
         $historicoData['aluno_id'] = $agendamento -> aluno_id;
         $historicoData['link_aluno'] = 'agendamento/indexAluno';
         $historicoData['link_profissional'] = 'agendamento/create?data=' . human_date($agendamento -> data);

         Historico::create($historicoData);
     });
  }

   protected $table = 'agendamentos';

   protected $fillable = ['aluno_id', 'data', 'horario_disponivel_id','descricao'];

   public static function horarios_disponiveis($data){
      $results = DB::select("
      select hd.hora, hd.id, a.data, u.id as aluno_id, u.name as nome, a.id as agendamento_id, a.descricao
         from horarios_disponiveis hd
         left join agendamentos a
            on a.horario_disponivel_id = hd.id
         left join users u
            on a.aluno_id = u.id
         where (data = ?
           or data is null)
         and hd.disponivel = 1
            ", [$data]);
      return $results;
   }

   public function horario(){
      return $this -> belongsTo('App\HorarioDisponivel', 'horario_disponivel_id');
   }
    //
}
