<?php

function db_date($human_date){
   if(empty($human_date))
      return $human_date;
   $db_date = implode("-",array_reverse(explode('/',$human_date)));
   return $db_date;
}

function human_date($db_date){
   if(empty($db_date))
      return $db_date;
   $human_date = implode("/",array_reverse(explode('-',$db_date)));
   return $human_date;
}

function meses_select(){
   $meses = [ 1 => 'Janeiro', 2 => 'Fevereiro', 3 => 'Março', 4 => 'Abril',5 => 'Maio', 6 => 'Junho', 7 => 'Julho', 8 => 'Agosto', 9 => 'Setembro', 10 => 'Outubro', 11 => 'Novembro', 12 => 'Dezembro' ];

   return $meses;

}

function anos_select(){
   $ano_atual = date('Y');
   $anos_range = range($ano_atual - 1, $ano_atual + 1);
   $anos = [];
   foreach ($anos_range as $key => $ano) {
      $anos[$ano] = $ano;
   }
   return $anos;
}

function head_treino($tipo_treino){
  switch ($tipo_treino) {
    case '4':
      return ['atividade' => 'Natação-estilo', 'distancia' => 'm'];
      break;
    default:
      return ['atividade' => 'Atividade', 'distancia' => 'Km'];
      break;
  }
}
