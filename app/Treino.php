<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Treinamento;
use App\Atividade;

class treino extends Model
{

   protected $fillable = ['treinamento_id','anotacoes','titulo_treino','sigla'];

   public function __construct($attributes = array(), $new_atividades = false)  {
        parent::__construct($attributes);
        if($new_atividades){
          // Pilates
          for($i = 0; $i < 8; $i++){
            $this -> atividades_pil -> push(new AtividadePil());
          }

          // Musculação
           for($i = 0; $i < 8; $i++){
              $this -> atividades_mus -> push(new AtividadeMus());
           }
          // Restante dos treinamentos
           for($i = 0; $i < 8; $i++){
              $this -> atividades -> push(new Atividade());
           }
        }
   }

  public function atividades_pil(){
     return $this -> hasMany('App\AtividadePil','treino_id');
  }

  public function atividades_mus(){
     return $this -> hasMany('App\AtividadeMus','treino_id');
  }


   public function treinamento() {
     return $this -> belongsTo('App\Treinamento');
   }

   public function atividades(){
      return $this -> hasMany('App\Atividade');
   }
    //
}
