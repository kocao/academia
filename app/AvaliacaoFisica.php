<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvaliacaoFisica extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'avaliacoes_fisicas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
     public static function boot()
     {
        parent::boot();

        static::created(function($treinamento)
        {
            $historicoData['descricao'] = "Foi criada uma nova Avaliação física para você.";
            $historicoData['profissional_id'] = $treinamento -> profissional_id;
            $historicoData['data'] = date('Y-m-d');
            $historicoData['aluno_id'] = $treinamento -> aluno_id;
            $historicoData['link_aluno'] = 'avaliacao-aluno';
            $historicoData['link_profissional'] = 'avaliacao-fisica/' . $treinamento -> id;


            Historico::create($historicoData);

        });

        static::updated(function($treinamento)
        {
          $historicoData['descricao'] = "Você teve uma Avaliação Física atualizada.";
          $historicoData['profissional_id'] = $treinamento -> profissional_id;
          $historicoData['data'] = date('Y-m-d');
          $historicoData['aluno_id'] = $treinamento -> aluno_id;
          $historicoData['link_aluno'] = 'avaliacao-aluno';
          $historicoData['link_profissional'] = 'avaliacao-fisica'. $treinamento -> id;

          Historico::create($historicoData);

        });
     }


   public function aluno(){
     return $this -> belongsTo('App\User','aluno_id');
   }

   public function profissional(){
     return $this -> belongsTo('App\User','profissional_id');
   }

    protected $fillable = ['peso', 'aluno_id', 'profissional_id', 'estatura', 'gor_ideal', 'gor_atual', 'peso_magro', 'peso_gordo', 'peso_residual', 'peso_muscular', 'per_muscular', 'torax', 'cintura', 'abdomen', 'quadril', 'coxa_direita', 'coxa_esquerda', 'pantu_direita', 'pantu_esquerda', 'ante_direito', 'ante_esquerdo', 'braco_direito', 'braco_esquerdo', 'subscapular', 'tricipital', 'peitoral', 'axilar', 'supra_iliaca', 'abdominal', 'coxa','peso_desejavel','data'];


}
