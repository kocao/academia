<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('aluno', function ($user) {
           return $user -> perfil == "Aluno";
        });

        Gate::define('admin', function ($user) {
           return $user -> perfil == "Administrador";
        });

        Gate::define('profissional', function ($user) {
           return $user -> perfil == "Profissional";
        });

        Gate::define('adminprof', function ($user) {
           return ($user -> perfil == "Profissional" || $user -> perfil == "Administrador");
        });

    }
}
