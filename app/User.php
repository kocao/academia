<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;
use App\AreaProfissional;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   //  protected $fillable = [
   //      'name', 'email', 'password',
   //  ];
    protected $fillable = ['ativo', 'perfil', 'name', 'email', 'id_area_profissional', 'registro_profissional',
    'telefone', 'whatsap', 'cpf', 'dt_nasc', 'password','provider','provider_id',
'contato_emer','medico','tipo_sang','alergias','plano_saude','hospital','concordo'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public static function criar_aluno($data){
      // $user = User::create([
      //     'name' => $data['name'],
      //     'email' => $data['email'],
      //     'password' => bcrypt($data['password']),
      // ]);
      // $data['password'] = bcrypt($data['password']);
      // $data['dt_nasc'] = db_date($data['dt_nasc']);
      // $user = User::create($data),
      // ]);
      return $user;

   }

   public function area_profissional(){
      return $this -> belongsTo('App\AreaProfissional','id_area_profissional');
   }

   public static function alunosSelect(){
     $alunos = [];
     foreach (User::where("perfil" ,"=", 'Aluno')->get() as $key => $aluno) {
        $alunos[$aluno -> id] = $aluno -> name;
     }

     return $alunos;
   }

   public static function profissionaisSelect(){
     $profissionais = [];
     foreach (User::where("perfil" ,"=", 'Profissional')->get() as $key => $profissional) {
        $profissionais[$profissional -> id] = $profissional -> name;
     }

     return $profissionais;
   }

}
