<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\DietaRefeicao;
class Dieta extends Model
{
  public static function boot()
  {
     parent::boot();

     static::created(function($dieta)
     {
       if(empty($dieta -> id)){
         $historicoData['descricao'] = "Foi criada uma nova Dieta para você.";
       } else {
         $historicoData['descricao'] = "Você teve uma Dieta atualizada.";
       }
       $historicoData['profissional_id'] = $dieta -> id_profissional;
       $historicoData['data'] = date('Y-m-d');
       $historicoData['aluno_id'] = $dieta -> id_aluno;
       $historicoData['link_aluno'] = 'dieta/' . $dieta -> id;
       $historicoData['link_profissional'] = 'dieta/' . $dieta -> id;


       Historico::create($historicoData);

     });
  }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    const const_refeicoes = ['Desjejum','Lanche','Almoço','Lanche','Jantar','Ceia'];
    protected $table = 'dietas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id_profissional', 'id_aluno', 'data', 'descricao'];

    public function aluno(){
      return $this -> belongsTo('App\User','id_aluno');
    }

    public function dieta_refeicoes(){
      return $this -> hasMany('App\DietaRefeicao');
   }

    public static function refeicoes(){
      return self::const_refeicoes;
    }


}
