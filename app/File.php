<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
   protected $fillable = ['id_exame', 'id_user', 'path_arquivo','url_arquivo'];

   public function deleteImage(){
      unlink($this -> path_arquivo);
      $this -> destroy($this -> id);
   }
}
