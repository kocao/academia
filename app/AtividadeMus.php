<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AtividadeMus extends Model
{
   protected $table = 'atividades_mus';
   protected $fillable = ["treino_id","exercicio","serie",
"repeticao", "carga", "ajustes", "pausa"];
}
