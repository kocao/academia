<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HorarioDisponivel extends Model
{
    protected $table = "horarios_disponiveis";

    protected $fillable = ['hora','disponivel'];

    public function agendamentos(){
      return $this -> hasMany('App\Agendamento');
    }
}
