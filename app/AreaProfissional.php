<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaProfissional extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'area_profissional';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['descr_area', 'sigla_registro', 'registro'];


    public static function areaSelect(){
      $areas = [];
      foreach (AreaProfissional::all() as $key => $area) {
         $areas[$area -> id] = $area -> descr_area;
      }

      return $areas;
    }


}
