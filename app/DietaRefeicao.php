<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DietaRefeicao extends Model
{
    protected $table = "dieta_refeicoes";

    protected $fillable = ['segunda','terca','quarta','quinta','sexta','sabado','domingo','opcao1','opcao2','opcao3','ordem_refeicao','dieta_id'];

}
