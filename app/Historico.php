<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historico extends Model
{
    protected $table = "historico";
    protected $fillable = ["data","profissional_id","aluno_id","descricao", 'link_aluno', 'link_profissional'];

    public function aluno(){
      return $this -> belongsTo('App\User','aluno_id');
    }
    public function profissional(){
      return $this -> belongsTo('App\User','profissional_id');
    }
}
