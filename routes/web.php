<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
   return redirect('user');
   //  return view('ProfissionalController@index');
});

// Route::controller('delete', 'ExameController');
Route::get('/exame/delete/{id}', array( 'uses' => 'ExameController@delete'))->name('exame.delete')->middleware('auth');

// Route::resource('admin/profissional', 'ProfissionalController')->middleware('auth');
//
Route::resource('admin/area-profissional', 'AreaProfissionalController')->middleware('auth');




Auth::routes();

/* Exames */
Route::get('exame-index','ExameController@index_profissional')->middleware('auth');
Route::resource('exame', 'ExameController')->middleware('auth');

Route::post('file/delete', array('as' => 'validate', 'uses' => 'ExameController@delete_file'))->middleware('auth');
// Route::delete('file/delete/{id}',  'ExameController@delete_file');

/*
GET|HEAD  | exame/delete/{id}                                | exame.delete              | App\Http\Controllers\ExameController@delete                            | web,auth
*/

Route::resource('dieta', 'DietaController')->middleware('auth');
Route::resource('avaliacao-fisica', 'AvaliacaoFisicaController')->middleware('auth');
Route::get('avaliacao-aluno', array('uses' => 'AvaliacaoFisicaController@indexAluno'))->middleware('auth');
/* Treinamento */
Route::resource('treinamento', 'TreinamentoController')->middleware('auth');
Route::get('treinamento/tipo_treino/{tipo_treino}',['uses' => 'TreinamentoController@indexAluno']) -> middleware('auth');
Route::post('view_treino', ['uses' => 'TreinamentoController@view_treino']);
Route::post('modal_periodo', ['uses' => 'TreinamentoController@modal_periodo']);

/* Agendamento */
Route::get('agendamento/create', array('uses' => 'AgendamentoController@create'))->middleware('auth');
Route::get('agendamento/indexAluno', array('uses' => 'AgendamentoController@indexAluno'))->middleware('auth');
Route::post('horarios_view', array('uses' => 'AgendamentoController@getHorariosView'))->middleware('auth');
Route::post('agendamento', array('uses' => 'AgendamentoController@store'))->middleware('auth');
Route::delete('agendamento/{id}', array('uses' => 'AgendamentoController@destroy'))->middleware('auth');
Route::post('agendamento', array('uses' => 'AgendamentoController@store'))->middleware('auth');


/* Horários */
Route::get('horarios_disponiveis', ['uses' => 'HorarioDisponivelController@index']) -> middleware('auth');


/* Financeiro*/
Route::get('financeiro', ['uses' => 'FinanceiroController@index']) -> middleware('auth');
Route::post('financeiro', ['uses' => 'FinanceiroController@store']) -> middleware('auth');
Route::post('add_conta', ['uses' => 'FinanceiroController@add_conta']) -> middleware('auth');
Route::post('add_receber', ['uses' => 'FinanceiroController@add_receber']) -> middleware('auth');
Route::delete('financeiro/{id}', array('uses' => 'FinanceiroController@destroy'))->middleware('auth');

/*Usuários*/
Route::resource('user', 'UserController')->middleware('auth');
Route::get('dados-pessoais',['uses' => 'UserController@dados_pessoais'])->middleware('auth');
Route::get('edit-aluno',['uses' => 'UserController@edit_aluno'])->middleware('auth');
Route::patch('update-aluno',['uses' => 'UserController@update_aluno'])->middleware('auth');

/* Histórico */
Route::get('historico',['uses' => 'HistoricoController@index'])->middleware('auth');


/* Redes Sociais */
Route::get('loginFacebook', 'RedesSociaisController@loginFacebook');
Route::get('facebook', 'RedesSociaisController@pageFacebook');

Route::get('loginGoogle', 'RedesSociaisController@loginGoogle');
Route::get('google', 'RedesSociaisController@pageGoogle');
